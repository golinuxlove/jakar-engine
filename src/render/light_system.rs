use core::resource_management::asset_manager::AssetManager;
use core::next_tree::{SceneComparer, ValueTypeBool, SceneTree, project_points, get_max_xy_len, JakarNode};
use core::next_tree::content::ContentType;

use core::resources::camera::Camera;

use jakar_threadpool::ThreadPool;

use render::shader::shader_inputs::lights::ty::{LightCount, PointLight, SpotLight, DirectionalLight};
use render::renderer::JkQueues;

use vulkano::descriptor::descriptor_set::DescriptorSet;
use vulkano::descriptor::descriptor_set::PersistentDescriptorSet;
use vulkano::buffer::*;
use render::shader::shader_inputs::lights;
use vulkano::buffer::cpu_pool::CpuBufferPoolSubbuffer;
use vulkano::buffer::immutable::ImmutableBuffer;
use vulkano::buffer::BufferUsage;
use vulkano::sync::GpuFuture;
use vulkano;

use collision::*;

use std::sync::Arc;


/// Stores all lights which are needed in the current frame.
pub struct LightStore {
    // since we generated ordered lists of needed lights and their shader_info pendants we store them
    // here.
    pub point_lights: Vec<PointLight>,
    pub directional_lights: Vec<DirectionalLight>,
    pub spot_lights: Vec<SpotLight>
}

impl LightStore{
    pub fn new() -> Self{
        LightStore{
            point_lights: Vec::new(),
            directional_lights: Vec::new(),
            spot_lights: Vec::new(),
        }
    }
}

///The system is responsible for everything that has to do with actual light (no shadows). Itn will
/// dispatch a compute shader which builds a 3d matrix in worldspace which holds the following information
/// at each entry:
/// - how many point lights
/// - the indices of these point lights in the point_ligh_list
/// - how many spot lights
/// - the indices of these spot lights in the point_ligh_list
///
/// This information is used in the forward pass to determin which lights needs to be considered when shading.
/// because of this optimization it is possible to use around 1000 spot or point lights while still maintaining
/// over 30fps on a mid range gpu.
pub struct LightSystem {
    queue: JkQueues,
    //Stores the nodes and shader infos we got from the shadow_system
    light_store: LightStore,

    //is the buffer of currently used point, directional and spotlights used
    current_point_light_list: Arc<ImmutableBuffer<[lights::ty::PointLight]>>,
    current_dir_light_list: Arc<ImmutableBuffer<[lights::ty::DirectionalLight]>>,
    current_spot_light_list: Arc<ImmutableBuffer<[lights::ty::SpotLight]>>,
    current_light_count: CpuBufferPoolSubbuffer<lights::ty::LightCount, Arc<vulkano::memory::pool::StdMemoryPool>>,
    //Pool to create the light count buffer.
    buffer_pool_05_count: vulkano::buffer::cpu_pool::CpuBufferPool<lights::ty::LightCount>,
}

impl LightSystem{
    pub fn new(
        device: Arc<vulkano::device::Device>,
        queue: JkQueues,
    ) -> Self {

        //Now we pre_create the first current buffers and store them, they will be updated each time
        //a compute shader for a new frame is dispatched
        let (c_point_light, c_dir_light, c_spot_lights) = {
            //Now build the buffers from the shader_infos and update them internaly
            let p_vec = Vec::new();
            let s_vec = Vec::new();
            let d_vec = Vec::new();

            let p_l = {
                let (buffer, future) = ImmutableBuffer::from_iter(
                    p_vec.clone().into_iter(),
                    BufferUsage::all(),
                    queue.transfer.clone()
                ).expect("Failed to create point light buffer");
                //Now drop the future (which will execute and then return)
                drop(future);
                buffer
            };

            let s_l = {
                let (buffer, future) = ImmutableBuffer::from_iter(
                    s_vec.clone().into_iter(),
                    BufferUsage::all(),
                    queue.transfer.clone()
                ).expect("Failed to create spot light buffer");
                //Now drop the future (which will execute and then return)
                drop(future);
                buffer

            };
            let d_l = {
                let (buffer, future) = ImmutableBuffer::from_iter(
                    d_vec.clone().into_iter(),
                    BufferUsage::all(),
                    queue.transfer.clone()
                ).expect("Failed to create directional light buffer");
                //Now drop the future (which will execute and then return)
                drop(future);
                buffer
            };

            (p_l, d_l, s_l)
        };


        let tmp_uniform_buffer_pool_05 = CpuBufferPool::<lights::ty::LightCount>::new(
            device.clone(), BufferUsage::all()
        );

        let light_count_tmp = lights::ty::LightCount{
            points: 0,
            directionals: 0,
            spots: 0,
            _pad0: 0,
        };

        let c_light_count = tmp_uniform_buffer_pool_05
        .next(light_count_tmp).expect("Failed to alloc first light count buffer");

        LightSystem{
            queue: queue,

            light_store: LightStore::new(),

            current_point_light_list: c_point_light,
            current_dir_light_list: c_dir_light,
            current_spot_light_list: c_spot_lights,
            current_light_count: c_light_count,
            buffer_pool_05_count: tmp_uniform_buffer_pool_05,
        }
    }

    ///Analyses the lights we currently need, sends the to the shadow system to decide which light
    /// gets a shadow, and where. Then builds the uniform buffers for the lights which get used
    /// in the compute and shadow passes.
    pub fn update_light_set(
        &mut self,
        asset_manager: &mut AssetManager,
        thread_pool: &mut ThreadPool,
    )-> Box<GpuFuture + Send + Sync>{

        self.light_store = self.find_light_store(
            asset_manager,
            thread_pool
        );

        //Now create a buffer from theese lights
        let light_counts = LightCount{
            points: self.light_store.point_lights.len() as u32,
            directionals: self.light_store.directional_lights.len() as u32,
            spots: self.light_store.spot_lights.len() as u32,
            _pad0: 0,
        };



        //Now build the buffers from the shader_infos and update them internaly
        //Todo this we copy the shader infos and push them into the following buffers
        let (points, directionals, spots) = {
            let p_lights = {
                let mut p_vec = Vec::new();
                for info in self.light_store.point_lights.iter(){
                    p_vec.push(info.clone());
                }
                p_vec
            };

            let d_lights = {
                let mut d_vec = Vec::new();
                for info in self.light_store.directional_lights.iter(){
                    d_vec.push(info.clone());
                }
                d_vec
            };

            let s_lights = {
                let mut s_vec = Vec::new();
                for info in self.light_store.spot_lights.iter(){
                    s_vec.push(info.clone());
                }
                s_vec
            };
            (p_lights, d_lights, s_lights)
        };


        let (new_point_light_list, point_future) = {
            let (buffer, future) = ImmutableBuffer::from_iter(
                points.into_iter(),
                BufferUsage::all(),
                self.queue.transfer.clone()
            ).expect("Failed to create point light buffer");
            //Now drop the future (which will execute and then return)
            (buffer, Box::new(future) as Box<GpuFuture + Send + Sync>)
        };

        let (new_spot_light_list, spot_future) = {
            let (buffer, future) = ImmutableBuffer::from_iter(
                spots.into_iter(),
                BufferUsage::all(),
                self.queue.transfer.clone()
            ).expect("Failed to create spot light buffer");
            //Now drop the future (which will execute and then return)
            (buffer, Box::new(future) as Box<GpuFuture + Send + Sync>)
        };
        let (new_dir_light_list, dir_future) = {
            let (buffer, future) = ImmutableBuffer::from_iter(
                directionals.into_iter(),
                BufferUsage::all(),
                self.queue.transfer.clone()
            ).expect("Failed to create directional light buffer");
            //Now drop the future (which will execute and then return)
            (buffer, Box::new(future) as Box<GpuFuture + Send + Sync>)
        };

        self.current_point_light_list = new_point_light_list;
        self.current_spot_light_list = new_spot_light_list;
        self.current_dir_light_list = new_dir_light_list;

        //And finally allocate a new buffer of light counts which describes the buffers above
        self.current_light_count = self.buffer_pool_05_count.next(
            light_counts
        ).expect("Failed to allocate new light count buffer");

        let inter: Box<GpuFuture + Send + Sync> = Box::new(point_future.join(spot_future));
        let ret_future: Box<GpuFuture + Send + Sync> = Box::new(inter.join(dir_future));
        ret_future
    }

    /// updates the information for which light are used in the frame
    pub fn find_light_store(
        &mut self,
        asset_manager: &mut AssetManager,
        thread_pool: &mut ThreadPool,
    )-> LightStore {
        // Going to compute which lights should cast a shadow, then build the light lists

        //This struct will we send back later containing a list of each light and its shader info
        let mut light_store = LightStore::new();

        let comparer = SceneComparer::new();

        let camera_mvp = asset_manager.get_camera().get_view_projection_matrix();
        let camera_frustum = asset_manager.get_camera().get_frustum_bound();
        let camera_vp = asset_manager.get_camera().get_view_projection_matrix();

        let shadow_casting_count ={
            let settings = asset_manager
            .get_settings();

            let read = settings.read().expect("failed to read engine settings");

            read.get_render_settings()
            .get_light_settings().shadow_settings.max_casting_lights.clone()
        };
        let mut used_shadow_casters = 0;

        // Since directional lights see everything we always use all of them
        let directional_lights: Vec<_> = {
            asset_manager.get_active_scene().copy_all_nodes(
                &Some(
                    SceneComparer::new().with_value_type(
                        ValueTypeBool::none().with_directional_light()
                    )
                )
            )
        };

        //Since directional lights have the biggest impact on the scene we let each of them have a shadow
        for d_light in directional_lights.into_iter(){
            let rotation = d_light.get_attrib().transform.rot;
            let light = {
                match d_light.get_value(){
                    ContentType::DirectionalLight(ref dir) => dir.clone(),
                    _ => continue,
                }
            };
            let cast_shadow: i32 = if used_shadow_casters < shadow_casting_count {
                used_shadow_casters += 1;
                (used_shadow_casters - 1) as i32 //use the last index
            }else{
                -1
            };
            let shader_info = light.as_shader_info(&rotation, cast_shadow);
            light_store.directional_lights.push(shader_info);
        }

        //For the remaining point a spot lights we have to decide the shadow casting.
        //Number one creteria is: Bound is in view, all of thoose are ordered and the one with the biggest surface
        //on the screen is used, simmilar to the LOD system.


        let spot_and_point_lights: Vec<JakarNode> = asset_manager
        .get_active_scene()
        .copy_all_nodes(
            &Some(
                comparer.clone().with_value_type(
                    ValueTypeBool::none().with_point_light().with_spot_light()
                )
                //.with_cull_distance(0.05, camera_mvp)
            )
        );
        let mut in_frustum_nodes: Vec<JakarNode> = Vec::new();
        //For each light, find if it is in the frustum and split the collection
        let mut out_frustum_nodes = Vec::new();

        for node in spot_and_point_lights.into_iter(){
            let relation = camera_frustum.contains(node.get_attrib().get_value_bound());
            if (relation == Relation::Cross) || (relation == Relation::In){
                in_frustum_nodes.push(node);
            }else{
                out_frustum_nodes.push(node);
            }
        }
        //Now sort the lights based on their impact on the screen
        let mut in_frustum_with_surface: Vec<(f32, JakarNode)> = Vec::new();
        for node in in_frustum_nodes.into_iter(){
            let mut points = node.get_attrib().get_value_bound().to_corners().to_vec();
            project_points(&mut points, &camera_vp);
            let dist = get_max_xy_len(&points);
            in_frustum_with_surface.push((dist, node));
        }
        //Now sort them based on the impact
        in_frustum_with_surface.as_mut_slice()
        .sort_unstable_by(|(surf_a, node_a), (surf_b, node_b)| if let Some(cmp) = surf_a.partial_cmp(surf_b){
            cmp
        }else{
            use std::cmp::Ordering;
            Ordering::Equal
        });

        for node in out_frustum_nodes.into_iter(){
            let light_location = node.get_attrib().transform.disp;
            let light_rotation = node.get_attrib().transform.rot;
            match node.get_value(){
                ContentType::SpotLight(ref light) => {
                    let shader_info = light.as_shader_info(&light_rotation, &light_location, -1);
                    light_store.spot_lights.push(shader_info);
                },
                ContentType::PointLight(ref light) => {
                    let shader_info = light.as_shader_info(&light_location, -1);
                    light_store.point_lights.push(shader_info);
                }
                _ => continue, //Is no pointlight, test next
            }
        }

        //now give shadow indices as long as possible and finally add them to the right vec again
        for (surf, node) in in_frustum_with_surface.into_iter(){
            let this_index = if used_shadow_casters < shadow_casting_count{
                used_shadow_casters += 1;
                (used_shadow_casters - 1) as i32
            }else{
                -1 //no more shadows
            };
            //Noice one more shadow light
            let light_location = node.get_attrib().transform.disp;
            let light_rotation = node.get_attrib().transform.rot;
            match node.get_value(){
                ContentType::SpotLight(ref light) => {
                    let shader_info = light.as_shader_info(&light_rotation, &light_location, this_index);
                    light_store.spot_lights.push(shader_info);
                },
                ContentType::PointLight(ref light) => {
                    let shader_info = light.as_shader_info(&light_location, this_index);
                    light_store.point_lights.push(shader_info);
                }
                _ => continue, //Is no pointlight, test next
            }

        }
        //Return the final light store
        light_store
    }

    ///Returns a reference to the current light store. This stores all currently used lights as well
    /// as the used shader information
    pub fn get_light_store(&mut self) -> &mut LightStore{
        &mut self.light_store
    }

    ///Since all the objects drawn in the current frame need to get the same light info, we create
    /// one decriptorset based on the needed set id when asked for it.
    ///TODO: Is only needed for the diffuse phase for now, can't we put this in a ring buffer?
    ///NOTE:
    /// - Binding 0 = point lights
    /// - Binding 1 = directional lights
    /// - Binding 2 = spot lights
    /// - Binding 3 = struct which describes how many actual lights where send
    pub fn get_light_descriptorset(
        &self,
        binding_id: u32,
        pipeline: Arc<vulkano::pipeline::ComputePipelineAbstract + Send + Sync>,
    ) -> Arc<DescriptorSet + Send + Sync>{
        let new_set = Arc::new(PersistentDescriptorSet::start(
                pipeline.clone(), binding_id as usize
            )
            .add_buffer(self.current_point_light_list.clone())
            .expect("Failed to add point lights to descriptor set")
            .add_buffer(self.current_dir_light_list.clone())
            .expect("Failed to add dir lights to descriptor set")
            .add_buffer(self.current_spot_light_list.clone())
            .expect("Failed to add spot lights to descriptor set")
            .add_buffer(self.current_light_count.clone())
            .expect("Failed to add light count to descriptor set")
            .build().expect("failed to build descriptor 04")
        );

        new_set
    }
}
