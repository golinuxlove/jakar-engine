use vulkano::pipeline::{ComputePipelineAbstract, ComputePipeline};
use vulkano::device::Device;
use vulkano::command_buffer::AutoCommandBufferBuilder;
use vulkano::descriptor::descriptor_set::FixedSizeDescriptorSetsPool;
use vulkano::buffer::cpu_pool::CpuBufferPool;
use vulkano::sampler::Sampler;
use vulkano::image::Dimensions;
use vulkano::buffer::BufferAccess;

use std::sync::Arc;
use std::time::Instant;

use tools::math::time_tools;
use render::renderer::JkCommandBuff;
use render::shader::shader_inputs::ray_tracing;
use core::next_tree::mesh_bvh::MeshBvh;
use render::frame_system::FrameSystem;
use render::light_system::LightSystem;

const SHADOWTHREADSIZE:[u32; 2] = [8; 2];



///Does all the shadow ray tracing. However, currently I am experimenting here.
pub struct ShadowRayTracing {
    pipeline_dir: Arc<ComputePipelineAbstract + Send + Sync>,
    pipeline_else: Arc<ComputePipelineAbstract + Send + Sync>,

    descriptor_pool_dir: FixedSizeDescriptorSetsPool<Arc<ComputePipelineAbstract + Send + Sync>>,
    descriptor_pool_else: FixedSizeDescriptorSetsPool<Arc<ComputePipelineAbstract + Send + Sync>>,
    gbuffer_sampler: Arc<Sampler>,
}

impl ShadowRayTracing{
    pub fn new(device: Arc<Device>, gbuffer_sampler: Arc<Sampler>) -> Self{

        println!("\tLoading shadow ray shader", );
        let shader_dir = Arc::new(directional_shadow_shader::Shader::load(device.clone())
            .expect("failed to create shader module directional shadows"));

        let shader_else = Arc::new(else_shadow_shader::Shader::load(device.clone())
            .expect("failed to create shader module else shadows"));

        println!("\tLoading shadow ray pipeline", );
        let shadow_pipeline_dir: Arc<ComputePipelineAbstract + Send + Sync> = Arc::new(
            ComputePipeline::new(device.clone(), &shader_dir.main_entry_point(), &()
        )
        .expect("failed to create compute pipeline directional shadows"));

        let shadow_pipeline_else: Arc<ComputePipelineAbstract + Send + Sync> = Arc::new(
            ComputePipeline::new(device.clone(), &shader_else.main_entry_point(), &()
        )
        .expect("failed to create compute pipeline else shadows"));

        println!("\tLoading shadow ray descriptor set", );
        let desc_pool_dir = FixedSizeDescriptorSetsPool::new(shadow_pipeline_dir.clone(), 0);
        let desc_pool_else = FixedSizeDescriptorSetsPool::new(shadow_pipeline_else.clone(), 0);


        ShadowRayTracing{
            pipeline_dir: shadow_pipeline_dir,
            pipeline_else: shadow_pipeline_else,
            descriptor_pool_dir: desc_pool_dir,
            descriptor_pool_else: desc_pool_else,
            gbuffer_sampler: gbuffer_sampler,
        }
    }

    pub fn render_debug_shadow(
        &mut self,
        command_buffer: &mut JkCommandBuff,
        frame_sys: &FrameSystem,
        light_sys: &LightSystem,
        top_bvh_buffer: Arc<BufferAccess + Send + Sync>,
        bottom_bvh_buffer: Arc<BufferAccess + Send + Sync>,
        model_buffer: Arc<BufferAccess + Send + Sync>,
        face_buffer: Arc<BufferAccess + Send + Sync>,
        vertex_buffer: Arc<BufferAccess + Send + Sync>,
    ){
        let position_img = frame_sys.get_passes().images.gbuffer.position.clone();
        let normal_img = frame_sys.get_passes().images.gbuffer.normal.clone();
        let shadow_img = frame_sys.get_passes().images.ray_tracing.shadows.clone();
        let debug_img = frame_sys.get_passes().images.ray_tracing.debug.clone();
        let image_dims: [u32; 2] = {
            match shadow_img.dimensions(){
                Dimensions::Dim2d{width, height} => [width, height],
                Dimensions::Dim2dArray{width, height, array_layers: _} => [width, height],
                _ => {
                    println!("Faking storage image size...", );
                    [1,1]
                }
            }
        };
        let dispatch_dims = [
            (image_dims[0] / SHADOWTHREADSIZE[0]) + 1,
            (image_dims[1] / SHADOWTHREADSIZE[1]) + 1,
            1
        ];

        //Create descriptor for the shadow pass
        let descriptor = self.descriptor_pool_dir.next()
        .add_sampled_image(
            position_img.clone(), self.gbuffer_sampler.clone()
        ).expect("failed to add position image to shadow pass")
        .add_sampled_image(
            normal_img.clone(), self.gbuffer_sampler.clone()
        ).expect("failed to add normal image to shadow pass")
        .add_image(shadow_img.clone())
        .expect("failed to add target shadow image to shadow pass")
        .add_buffer(top_bvh_buffer.clone())
        .expect("failed to add top bvh buffer to compute pass")
        .add_buffer(model_buffer.clone())
        .expect("failed to add modell buffer to compute pass")
        .add_buffer(bottom_bvh_buffer.clone())
        .expect("failed to add bottom bvh buffer to compute pass")
        .add_buffer(face_buffer.clone())
        .expect("failed to add face buffer to compute pass")
        .add_buffer(vertex_buffer.clone())
        .expect("failed to add vertex buffer to compute pass")
        .add_image(debug_img.clone())
        .expect("failed to add debug image!")
        .build().expect("failed to build bvh buffer");

        let light_descriptor = light_sys.get_light_descriptorset(1, self.pipeline_dir.clone());

        //now start the compute pass.
        //We trace one ray per thread per pixel, since one thread group is 8x8 we devide the
        //work groupe size by pixel_count / 8


        let dir_cb = command_buffer.get_compute_buffer();
        let new_dir_cb = dir_cb.dispatch(
            dispatch_dims, self.pipeline_dir.clone(),
            (descriptor,light_descriptor), ()
        ).expect("failed to start directional shadow compute shader");
        //Execute on Compute queue
        command_buffer.set_compute_buffer(new_dir_cb);

        //Now start the point and spot light compute shader
        let point_desc = self.descriptor_pool_else.next()
        .add_sampled_image(
            position_img, self.gbuffer_sampler.clone()
        ).expect("failed to add position image to shadow pass")
        .add_sampled_image(
            normal_img, self.gbuffer_sampler.clone()
        ).expect("failed to add normal image to shadow pass")
        .add_image(shadow_img)
        .expect("failed to add target shadow image to shadow pass")

        .add_buffer(top_bvh_buffer)
        .expect("failed to add top bvh buffer to compute pass")
        .add_buffer(model_buffer)
        .expect("failed to add modell buffer to compute pass")
        .add_buffer(bottom_bvh_buffer)
        .expect("failed to add bottom bvh buffer to compute pass")

        .add_buffer(face_buffer)
        .expect("failed to add face buffer to compute pass")

        .add_buffer(vertex_buffer)
        .expect("failed to add vertex buffer to compute pass")

        .add_image(debug_img)
        .expect("failed to add debug image!")
        .build().expect("failed to build bvh buffer");

        let point_light_desc = light_sys.get_light_descriptorset(1, self.pipeline_else.clone());
        //While Dir lights are traced on compute queue, trace point and spot lights on graphics queue
        let else_cb = command_buffer.get_graphics_buffer();
        let new_else_cb = else_cb.dispatch(
            dispatch_dims, self.pipeline_else.clone(),
            (point_desc, point_light_desc), ()
        ).expect("failed to start point light shadows compute shader");
        command_buffer.set_graphics_buffer(new_else_cb);
    }
}


///Links to the directional shadow raytracing shader
mod directional_shadow_shader{
    vulkano_shaders::shader!{
        ty: "compute",
        path: "data/shader/r_shadow_dir.comp"
    }
}

///Links to the shader which raytraces spot and point lights
mod else_shadow_shader{
    vulkano_shaders::shader!{
        ty: "compute",
        path: "data/shader/r_shadow_else.comp"
    }
}
