use vulkano::pipeline::{ComputePipelineAbstract, ComputePipeline};
use vulkano::device::Device;
use vulkano::command_buffer::AutoCommandBufferBuilder;
use vulkano::descriptor::descriptor_set::FixedSizeDescriptorSetsPool;
use vulkano::buffer::cpu_pool::CpuBufferPool;
use vulkano::sampler::Sampler;
use vulkano::image::Dimensions;
use vulkano::buffer::BufferAccess;

use cgmath::*;

use std::sync::Arc;


use render::frame_system::FrameSystem;
use render::renderer::JkCommandBuff;
use render::light_system::LightSystem;
use core::resource_management::asset_manager::AssetManager;
use core::resources::camera::Camera;


const DIFFUSETHREADSIZE:[u32; 2] = [16; 2];


///Is used for the main diffuse pass of the enigne. Lights are applied to the g buffer here for
///instance.
#[derive(Clone)]
pub struct Diffuse {
    pipeline: Arc<ComputePipelineAbstract + Send + Sync>,

    settings_buffer_pool: CpuBufferPool<diffuse_shader::ty::RayTracingSettings>,
    descriptor_pool: FixedSizeDescriptorSetsPool<Arc<ComputePipelineAbstract + Send + Sync>>,
    gbuffer_sampler: Arc<Sampler>,

    local_size: u32,
}


impl Diffuse{
    pub fn new(device: Arc<Device>, gbuffer_sampler: Arc<Sampler>) -> Self{
        let shader = Arc::new(diffuse_shader::Shader::load(device.clone())
            .expect("failed to create shader module diffuse"));

        let diffuse_pipeline: Arc<ComputePipelineAbstract + Send + Sync> = Arc::new(
            ComputePipeline::new(device.clone(), &shader.main_entry_point(), &()
        )
        .expect("failed to create compute pipeline"));

        let desc_pool = FixedSizeDescriptorSetsPool::new(diffuse_pipeline.clone(), 0);
        let settings_pool = CpuBufferPool::uniform_buffer(device.clone());

        //TODO find optimal site for gpu dynamicly
        let local_size = 5;

        Diffuse{
            pipeline: diffuse_pipeline,
            descriptor_pool: desc_pool,
            settings_buffer_pool: settings_pool,
            gbuffer_sampler: gbuffer_sampler,

            local_size: local_size as u32,
        }
    }

    pub fn shade_diffuse(
        &mut self,
        command_buffer: &mut JkCommandBuff,
        framesystem: &FrameSystem,
        light_system: &LightSystem,
        asset_manager: &mut AssetManager
    ){

        let local_cb = command_buffer.get_compute_buffer();

        let light_descriptor = light_system.get_light_descriptorset(
            1, //For set index 1
            self.pipeline.clone()
        );

        let target_dimensions = framesystem.get_passes().images.ray_tracing.diffuse.dimensions();
        let image_dims: [u32; 2] = {
            match target_dimensions{
                Dimensions::Dim2d{width, height} => [width, height],
                Dimensions::Dim2dArray{width, height, array_layers} => [width, height],
                _ => {
                    println!("Faking storage image size...", );
                    [1,1]
                }
            }
        };

        let camera_pos = asset_manager.get_camera().get_position();

        let settings = diffuse_shader::ty::RayTracingSettings{
            target_image_size: image_dims,
            viewPos: camera_pos.extend(1.0).into(),
            _dummy0: [0; 8],
        };

        let settings_buffer = self.settings_buffer_pool
        .next(settings).expect("failed to alloc new ray tracing settings");

        let albedo = framesystem.get_passes().images.gbuffer.albedo.clone();
        let position = framesystem.get_passes().images.gbuffer.position.clone();
        let normal = framesystem.get_passes().images.gbuffer.normal.clone();
        let roughness = framesystem.get_passes().images.gbuffer.roughness.clone();
        let metalness = framesystem.get_passes().images.gbuffer.metalness.clone();
        let ambient = framesystem.get_passes().images.post_images.final_ao.clone();
        let diffuse_target = framesystem.get_passes().images.ray_tracing.diffuse.clone();
        let shadows = framesystem.get_passes().images.ray_tracing.shadows.clone();

        let main_descriptor = self.descriptor_pool.next()
        .add_sampled_image(
            albedo,
            self.gbuffer_sampler.clone()
        ).expect("Failed to add albedo to diffuse stage")
        .add_sampled_image(
            position,
            self.gbuffer_sampler.clone()
        ).expect("Failed to add position to diffuse stage")
        .add_sampled_image(
            normal,
            self.gbuffer_sampler.clone()
        ).expect("Failed to add normal to diffuse stage")
        .add_sampled_image(
            roughness,
            self.gbuffer_sampler.clone()
        ).expect("Failed to add roughness to diffuse stage")
        .add_sampled_image(
            metalness,
            self.gbuffer_sampler.clone()
        ).expect("Failed to add metalness to diffuse stage")
        //TODO might change this to the already processed ssao image?
        .add_sampled_image(
            ambient,
            self.gbuffer_sampler.clone()
        ).expect("Failed to add ambient to diffuse stage")
        .add_image(
            diffuse_target
        )
        .expect("failed to add target raytracing image to diffuse stage")
        .add_sampled_image(
            shadows,
            self.gbuffer_sampler.clone()
        )
        .expect("failed to add target raytracing image to diffuse stage")
        .add_buffer(settings_buffer)
        .expect("failed to add raytracing settings buffer to diffuse stage!")
        .build().expect("failed to add main desecriptor for diffuse stage!");

        let dispatch_dims = [
            (image_dims[0] / DIFFUSETHREADSIZE[0]) + 1,
            (image_dims[1] / DIFFUSETHREADSIZE[1]) + 1,
            1
        ];
        //Lets execute the shader!
        let new_cb = local_cb.dispatch(dispatch_dims, self.pipeline.clone(), (main_descriptor, light_descriptor), ())
        .expect("failed to start diffuse compute shader");

        //Finally overide the cb
        command_buffer.set_compute_buffer(new_cb);
    }
}


mod diffuse_shader{
    vulkano_shaders::shader!{
        ty: "compute",
        path: "data/shader/diffuse.comp"
    }
}
