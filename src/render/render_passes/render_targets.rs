use std::sync::{Arc, RwLock};

use vulkano::framebuffer::RenderPassAbstract;
use vulkano::device::Device;

use vulkano::image::attachment::AttachmentImage;
use vulkano::framebuffer::FramebufferAbstract;
use vulkano::format::Format;
use vulkano::image::ImageUsage;
use vulkano::image::StorageImage;
use vulkano::image::Dimensions;
use vulkano;

use core::engine_settings::EngineSettings;
use render::render_passes::UsedFormats;
use render::renderer::JkQueues;


//Helper struct which collects the images needed for a single blur stage
#[derive(Clone)]
pub struct BlurStage {
    ///The source image which gets blured, inital color comes from a resize operation
    pub input_image: Arc<StorageImage<Format>>,
    ///After bluring the horizontal
    pub after_h_img: Arc<StorageImage<Format>>,
    ///After also bluring vertical and possibly adding a image from another stage
    pub final_image: Arc<StorageImage<Format>>
}

impl BlurStage{
    pub fn new(
        device: Arc<vulkano::device::Device>,
        queue: JkQueues,
        formats: UsedFormats,
        dimensions: [u32; 2]
    ) -> Self{

        let dims = Dimensions::Dim2d{
            width: dimensions[0],
            height: dimensions[1],
        };

        let transfer_usage = ImageUsage{
            transfer_destination: true,
            transfer_source: true,
            color_attachment: true,
            sampled: true,
            storage: true,
            ..ImageUsage::none()
        };

        let input_image = StorageImage::with_usage(
            device.clone(),
            dims,
            formats.bloom,
            transfer_usage,
            queue.get_families().into_iter()
        ).expect("failed to creat input image for blur stage");

        let after_h_img = StorageImage::with_usage(
            device.clone(),
            dims,
            formats.bloom,
            transfer_usage,
            queue.get_families().into_iter()
        ).expect("failed to create after_h image for blur stage!");

        let final_image = StorageImage::with_usage(
            device.clone(),
            dims,
            formats.bloom,
            transfer_usage,
            queue.get_families().into_iter()
        ).expect("failed to create final image for blur stage!");

        BlurStage{
            input_image: input_image,
            after_h_img: after_h_img,
            final_image: final_image,
        }
    }
}

///Collects all images used for the first opaque mesh part
#[derive(Clone)]
pub struct GBuffer {
    //Holds raw albedo information
    pub albedo: Arc<AttachmentImage<Format>>,
    ///Holds the current depth image
    pub depth: Arc<AttachmentImage<Format>>,
    pub roughness: Arc<AttachmentImage<Format>>,
    pub metalness: Arc<AttachmentImage<Format>>,
    ///Worldspace normals in high presission 16bit ?
    pub normal: Arc<AttachmentImage<Format>>,
    ///Worldspace position in 32bit
    pub position: Arc<AttachmentImage<Format>>,
    ///Ambient information
    pub ambient: Arc<AttachmentImage<Format>>,
}

impl GBuffer{
    ///Creates the opaque pass images from the current engine settings
    pub fn new(
        settings: Arc<RwLock<EngineSettings>>,
        device: Arc<Device>,
        formats: UsedFormats
    ) -> Self{
        let current_dimensions = {
            settings
            .read()
            .expect("failed to lock settings for frame creation")
            .get_dimensions()
        };

        //depth
        let depth = AttachmentImage::sampled_input_attachment(
            device.clone(),
            current_dimensions,
            formats.depth
        ).expect("failed to create depth buffer!");

        //albedo
        let albedo = AttachmentImage::sampled_input_attachment(device.clone(),
            current_dimensions,
            formats.albedo
        ).expect("failed to create albedo buffer!");
        //normal buffer
        let normal = AttachmentImage::sampled_input_attachment(device.clone(),
            current_dimensions,
            formats.normal
        ).expect("failed to create normal buffer!");
        //position
        let position = AttachmentImage::sampled_input_attachment(device.clone(),
            current_dimensions,
            formats.position
        ).expect("failed to create normal buffer!");

        //roghness
        let roghness = AttachmentImage::sampled_input_attachment(device.clone(),
            current_dimensions,
            formats.roughness
        ).expect("failed to create normal buffer!");
        //metalness
        let metalness = AttachmentImage::sampled_input_attachment(device.clone(),
            current_dimensions,
            formats.metallness
        ).expect("failed to create normal buffer!");

        let ao = AttachmentImage::sampled_input_attachment(
            device.clone(),
            current_dimensions,
            formats.ambient
        ).expect("failed to create ao buffer!");


        GBuffer {
            //Holds raw albedo information
            albedo: albedo,
            ///Holds the current depth image
            depth: depth,
            roughness: roghness,
            metalness: metalness,
            ///Worldspace normals in high presission 16bit ?
            normal: normal,
            position: position,
            ///Ambient information
            ambient: ao,
        }
    }
}

///Collects all transition images which represent a specific stage, for instance the opaque meshes with
/// reflections applied
#[derive(Clone)]
pub struct PostProcessImages {

    //Holds only "too bright" pixels
    pub bright_pixels: Arc<AttachmentImage<Format>>,

    pub raw_ao: Arc<AttachmentImage<Format>>,

    pub final_ao: Arc<AttachmentImage<Format>>,

    ///Bloom images which will be added together for the final bloom image added on top of the
    /// ouput.
    pub scaled_hdr: Vec<BlurStage>,
}

impl PostProcessImages{
    pub fn new(
        settings: Arc<RwLock<EngineSettings>>,
        device: Arc<Device>,
        queue: JkQueues,
        formats: UsedFormats,
    ) -> Self{

        let current_dimensions = {
            settings
            .read()
            .expect("failed to lock settings for frame creation")
            .get_dimensions()
        };
        //Usage which allows the image to be written, sampled and used for blitting operations
        let transfer_usage = ImageUsage{
            transfer_destination: true,
            transfer_source: true,
            color_attachment: true,
            sampled: true,
            storage: true,
            ..ImageUsage::none()
        };

        let bright_pixels = AttachmentImage::with_usage(
            device.clone(),
            current_dimensions,
            formats.bloom,
            transfer_usage
        ).expect("failed to create bright pixel image!");


        let raw_ao = AttachmentImage::sampled_input_attachment(
            device.clone(),
            current_dimensions,
            formats.ambient
        ).expect("failed to create raw ao image!");

        let final_ao = AttachmentImage::with_usage(
            device.clone(),
            current_dimensions,
            formats.ambient,
            transfer_usage
        ).expect("failed to create final image!");

        let scaled_hdr = create_hdr_level(
            settings.clone(),
            device.clone(),
            queue.clone(),
            formats
        );

        PostProcessImages{
            bright_pixels,
            raw_ao,
            final_ao,
            scaled_hdr,
        }
    }
}

///Collects all images which are used while processing the g buffer, like main diffuse image, reflection
///images and shadow passes.
#[derive(Clone)]
pub struct RayTracingImages {
    ///Holds the image after lightning calculation including sharp shadows1
    pub diffuse: Arc<StorageImage<Format>>,
    ///Holds mirror like reflections
    pub reflection: Arc<StorageImage<Format>>,
    ///Holds a shadow map
    pub shadows: Arc<StorageImage<Format>>,
    ///Can contains debug information+
    pub debug: Arc<StorageImage<Format>>,
}

impl RayTracingImages{
    pub fn new(
        settings: Arc<RwLock<EngineSettings>>,
        device: Arc<Device>,
        queue: JkQueues,
        formats: UsedFormats,
    )-> Self{
        let current_dimensions = {
            settings
            .read()
            .expect("failed to lock settings for frame creation")
            .get_dimensions()
        };

        let ray_usage = ImageUsage{
            transfer_destination: true,
            transfer_source: true,
            color_attachment: false,
            sampled: true,
            storage: true,
            ..ImageUsage::none()
        };

        let diffuse = StorageImage::with_usage(
            device.clone(),
            Dimensions::Dim2d{width: current_dimensions[0], height: current_dimensions[1]},
            formats.diffuse,
            ray_usage,
            queue.get_families().into_iter()
        ).expect("Failed to create main diffuse image");

        let reflection = StorageImage::with_usage(
            device.clone(),
            Dimensions::Dim2d{width: current_dimensions[0], height: current_dimensions[1]},
            formats.reflection,
            ray_usage,
            queue.get_families().into_iter()
        ).expect("Failed to create main reflections image");


        let array_depth = {
            settings.read().expect("failed to read settings").get_render_settings().get_light_settings().shadow_settings.max_casting_lights
        };

        let shadow_image_res = {
            let reduce = settings.read().expect("failed to lock settings")
            .get_render_settings().get_light_settings().shadow_settings.pass_resolution;
            let width = (current_dimensions[0] as f32 / reduce as f32).floor() as u32;
            let height = (current_dimensions[1] as f32 / reduce as f32).floor() as u32;
            Dimensions::Dim2dArray{width: width, height: height, array_layers: array_depth as u32}
        };

        let debug_img_res = {
            let reduce = settings.read().expect("failed to lock settings")
            .get_render_settings().get_light_settings().shadow_settings.pass_resolution;
            let width = (current_dimensions[0] as f32 / reduce as f32).floor() as u32;
            let height = (current_dimensions[1] as f32 / reduce as f32).floor() as u32;
            Dimensions::Dim2d{width: width, height: height}
        };


        let shadows = StorageImage::with_usage(
            device.clone(),
            shadow_image_res,
            formats.shadows,
            ray_usage,
            queue.get_families().into_iter()
        ).expect("Failed to create main shadows image");

        let debug = StorageImage::with_usage(
            device.clone(),
            debug_img_res,
            formats.debug_info,
            ray_usage,
            queue.get_families().into_iter()
        ).expect("Failed to create main shadows image");

        RayTracingImages{
            diffuse,
            reflection,
            shadows,
            debug
        }
    }
}


///Collects all the images we render. If the engine settings or resolution of an image changes it will
/// Recreate the attachment.
#[derive(Clone)]
pub struct ImageStore {
    ///Recreation infos
    settings: Arc<RwLock<EngineSettings>>,
    device: Arc<Device>,
    queue: JkQueues,
    formats: UsedFormats,

    //The actual gbuffer images
    pub gbuffer: GBuffer,

    ///Holds all images which have something to do with the raytracing stage
    pub ray_tracing: RayTracingImages,

    pub post_images: PostProcessImages,
}


impl ImageStore{
    ///Creates a new GBuffer from the supplied settings. When settings change the GBuffer can be
    /// updated from the renderpass via `update_x()`.
    pub fn new(
        settings: Arc<RwLock<EngineSettings>>,
        device: Arc<Device>,
        queue: JkQueues,
        formats: UsedFormats,
    ) -> Self{

        ImageStore {
            //Recreation infos
            settings: settings.clone(),
            device: device.clone(),
            queue: queue.clone(),
            formats: formats,
            //The actual gbuffer images
            gbuffer: GBuffer::new(
                settings.clone(),
                device.clone(),
                formats
            ),

            ray_tracing: RayTracingImages::new(
                settings.clone(),
                device.clone(),
                queue.clone(),
                formats
            ),

            post_images: PostProcessImages::new(
                settings.clone(),
                device.clone(),
                queue.clone(),
                formats
            ),
        }
    }

    ///Recreates all resolution dependent images
    pub fn recreate_res_dependent(&mut self){
        self.gbuffer = GBuffer::new(
            self.settings.clone(),
            self.device.clone(),
            self.formats
        );

        self.ray_tracing = RayTracingImages::new(
            self.settings.clone(),
            self.device.clone(),
            self.queue.clone(),
            self.formats
        );

        self.post_images = PostProcessImages::new(
            self.settings.clone(),
            self.device.clone(),
            self.queue.clone(),
            self.formats
        );
    }

    ///Returns the framebuffer for writing the the horizontal blured images for the level at idx.
    /// Returns the last / smales level if the idx is too big.
    pub fn get_fb_blur_h(
        &self,
        idx: usize,
        renderpass: Arc<RenderPassAbstract + Send + Sync>
    ) -> Arc<FramebufferAbstract + Send + Sync>{

        if self.post_images.scaled_hdr.is_empty(){
            panic!("The bloom images are empty, that should not happen");
        }

        let mut index = idx;
        if index > self.post_images.scaled_hdr.len() - 1{
            index = self.post_images.scaled_hdr.len() - 1;
        }

        let after_h_image = self.post_images.scaled_hdr[index].after_h_img.clone();

        Arc::new(
            vulkano::framebuffer::Framebuffer::start(renderpass)
            //Only writes to after h
            .add(after_h_image)
            .expect("failed to add after_blur_h image")
            .build()
            .expect("failed to build main framebuffer!")
        )
    }

    ///Retuirns the framebuffer to blur the h_blured image to the final stage
    pub fn get_fb_to_final(
        &self,
        idx: usize,
        renderpass: Arc<RenderPassAbstract + Send + Sync>
    ) -> Arc<FramebufferAbstract + Send + Sync> {

        if self.post_images.scaled_hdr.is_empty(){
            panic!("The bloom images are empty, that should not happen");
        }

        let mut index = idx;
        if index > self.post_images.scaled_hdr.len() - 1{
            index = self.post_images.scaled_hdr.len() - 1;
        }

        let final_image = self.post_images.scaled_hdr[index].final_image.clone();

        Arc::new(
            vulkano::framebuffer::Framebuffer::start(renderpass)
            //Only writes to after v
            .add(final_image)
            .expect("failed to add final blured image")
            .build()
            .expect("failed to build main framebuffer!")
        )
    }
}

///Creates scaled images which are used for eye adaption and bloom
fn create_hdr_level(
    settings: Arc<RwLock<EngineSettings>>,
    device: Arc<Device>,
    queue: JkQueues,
    formats: UsedFormats,
) -> Vec<BlurStage>{
    let current_dimensions = {
        let set_lck = settings
        .read()
        .expect("failed to lock settings for frame creation");
        let dur_dim = set_lck
        .get_dimensions();
        dur_dim
    };


    //For several PostProcess Stuff we might need scaled images of the original image
    // we do this by storing several layers of scaled images in this vec.
    //The first one in half size comapred to the original, the last one is a 1x1 texture
    let mut scaled_ldr_images = Vec::new();
    let mut new_dimension_image = current_dimensions;
    //Always use the dimension, create image, then scale down
    'reduction_loop: loop {

        //calculate the half dimension
        let mut new_dim_x = ((new_dimension_image[0] as f32).floor() / 2.0) as u32;
        let mut new_dim_y = ((new_dimension_image[1] as f32).floor() / 2.0) as u32;
        //Check if we reached 1 for only one but not the other, if so, cap at one
        if new_dim_x < 1 && new_dim_y >= 1{
            new_dim_x = 1;
        }

        if new_dim_y < 1 && new_dim_x >= 1{
            new_dim_y = 1;
        }

        new_dimension_image = [
            new_dim_x,
            new_dim_y
        ];

        //create the half image
        let new_image = BlurStage::new(
            device.clone(),
            queue.clone(),
            formats,
            new_dimension_image
        );
        //push it
        scaled_ldr_images.push(new_image);
        //println!("Aspect_last_frame: [{}, {}]", new_dimension_image[0], new_dimension_image[1]);

        //break if reached the 1x1 pixel
        if new_dim_x <= 1 && new_dim_y <= 1{
            break;
        }
    }

    scaled_ldr_images
}

///Creates a chain of images each scaled down by a factor of two.
pub fn create_mip_chain(
    settings: Arc<RwLock<EngineSettings>>,
    device: Arc<Device>,
    queue: JkQueues,
    formats: UsedFormats,
) -> Vec<Arc<StorageImage<Format>>>{
    let current_dimensions = {
        let set_lck = settings
        .read()
        .expect("failed to lock settings for frame creation");
        let dur_dim = set_lck
        .get_dimensions();
        dur_dim
    };

    //Now generate the targets for the average lumiosity pass.
    let transfer_usage = ImageUsage{
        transfer_destination: true,
        transfer_source: true,
        color_attachment: true,
        sampled: true,
        storage: true,
        ..ImageUsage::none()
    };
    //For several PostProcess Stuff we might need scaled images of the original image
    // we do this by storing several layers of scaled images in this vec.
    //The first one in half size comapred to the original, the last one is a 1x1 texture
    let mut scaled_ldr_images = Vec::new();
    let mut new_dimension_image = current_dimensions;
    //Always use the dimension, create image, then scale down
    'reduction_loop: loop {

        //calculate the half dimension
        let mut new_dim_x = ((new_dimension_image[0] as f32).floor() / 2.0) as u32;
        let mut new_dim_y = ((new_dimension_image[1] as f32).floor() / 2.0) as u32;
        //Check if we reached 1 for only one but not the other, if so, cap at one
        if new_dim_x < 1 && new_dim_y >= 1{
            new_dim_x = 1;
        }

        if new_dim_y < 1 && new_dim_x >= 1{
            new_dim_y = 1;
        }
        //Update the dimensions
        new_dimension_image = [new_dim_x, new_dim_y];

        //create the half image
        let new_image = StorageImage::with_usage(
            device.clone(),
            Dimensions::Dim2d{
                width: new_dim_x,
                height: new_dim_y,
            },
            formats.diffuse,
            transfer_usage,
            queue.get_families().into_iter()
        ).expect("failed to create scaled ldr image");

        //push it
        scaled_ldr_images.push(new_image);
        //println!("Aspect_last_frame: [{}, {}]", new_dimension_image[0], new_dimension_image[1]);


        //break if reached the 1x1 pixel
        if new_dim_x <= 1 && new_dim_y <= 1{
            break;
        }
    }

    scaled_ldr_images
}
