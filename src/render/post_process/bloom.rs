use vulkano;
use vulkano::command_buffer::AutoCommandBufferBuilder;
use vulkano::sampler::Sampler;
use vulkano::descriptor::descriptor_set::FixedSizeDescriptorSetsPool;
use vulkano::pipeline::ComputePipelineAbstract;
use vulkano::pipeline::ComputePipeline;
use vulkano::image::ImageDimensions;
use vulkano::image::traits::ImageAccess;
use vulkano::buffer::cpu_pool::CpuBufferPool;
use vulkano::buffer::BufferAccess;
use vulkano::pipeline::GraphicsPipelineAbstract;

use core::engine_settings::EngineSettings;
use render::frame_system::FrameSystem;
use render::pipeline_manager::PipelineManager;
use render::render_passes::render_targets::BlurStage;
use render::pipeline::Pipeline;
use render::pipeline_builder;
use render::shader::shaders::hdr_resolve;
use render::render_passes::{UsedFormats, RenderPassConf};
use render::post_process;
use render::renderer::JkCommandBuff;
use render::post_process::physical_camera::PhysicalCameraPass;

use std::sync::{Arc,RwLock};

pub const BLOOMTHREADSIZE: [u32; 2] = [8,8];

///Contains all components needed to generate the the bloom images
pub struct Bloom {
    engine_settings: Arc<RwLock<EngineSettings>>,
    screen_vertex_buffer: Arc<BufferAccess + Send + Sync>,
    screen_sampler: Arc<Sampler>,

    sorting_pipe: Arc<Pipeline>,
    sorting_desc_pool: FixedSizeDescriptorSetsPool<Arc<GraphicsPipelineAbstract + Send + Sync>>,

    blur_settings_pool: CpuBufferPool<blur_cmp_shader::ty::blur_settings>,
    blur_descset_pool: FixedSizeDescriptorSetsPool<Arc<ComputePipelineAbstract + Send + Sync>>,
    blur_comp_pipe: Arc<ComputePipelineAbstract + Send + Sync>,
}

impl Bloom{

    pub fn new(
        engine_settings: Arc<RwLock<EngineSettings>>,
        device: Arc<vulkano::device::Device>,
        pipeline_manager: Arc<RwLock<PipelineManager>>,
        screen_vertex_buffer: Arc<BufferAccess + Send + Sync>,
        screen_sampler: Arc<Sampler>,
        formats: UsedFormats
    ) -> Self{
        //Find the pipeline needed to sort hdr fragments
        let sorting_pipe = pipeline_manager.write()
        .expect("failed to lock new pipeline manager")
        .get_pipeline_by_config(
            pipeline_builder::PipelineConfig::default()
                .with_shader("PpResolveHdr".to_string())
                .with_render_pass(RenderPassConf::SimplePass(formats.bloom))
                .with_depth_and_stencil_settings(
                    pipeline_builder::DepthStencilConfig::NoDepthNoStencil
                ),
        );

        let sorting_desc_pool = FixedSizeDescriptorSetsPool::new(
            sorting_pipe.get_pipeline_ref().clone(), 0
        );


        let shader = Arc::new(blur_cmp_shader::Shader::load(device.clone())
            .expect("failed to create shader module"));

        let blur_comp_pipe: Arc<ComputePipelineAbstract + Send + Sync> = Arc::new(
            ComputePipeline::new(device.clone(), &shader.main_entry_point(), &()
        )
        .expect("failed to create compute pipeline"));

        let blur_descset_pool = FixedSizeDescriptorSetsPool::new(blur_comp_pipe.clone(), 0);
        let blur_settings_pool = CpuBufferPool::uniform_buffer(device.clone());

        Bloom{
            engine_settings,
            screen_vertex_buffer,
            screen_sampler,

            sorting_pipe,
            sorting_desc_pool,

            blur_settings_pool,
            blur_descset_pool,
            blur_comp_pipe
        }
    }

    pub fn execute_blur(&mut self,
        command_buffer: &mut JkCommandBuff,
        frame_system: &FrameSystem,
        sampler: Arc<Sampler>,
        physical_camera: &PhysicalCameraPass,
    ){

        //TODO since we blur via a compute kernel it might be nice to do this async.
        let mut cb = command_buffer.get_graphics_buffer();

        //Resize unitl we hit the set limit
        cb = self.resize(
            cb,
            frame_system,
            physical_camera
        );
        //Now blur in reversed order and add image up unil we hit the last image
        cb = self.blur(
            cb,
            frame_system,
            sampler,
        );

        command_buffer.set_graphics_buffer(cb);
    }

    fn resize(&mut self,
        command_buffer: AutoCommandBufferBuilder,
        frame_system: &FrameSystem,
        physical_camera: &PhysicalCameraPass,
    ) -> AutoCommandBufferBuilder{

        //First, do small pass which sorts out the hdr fragments only or whatever,
        let target_image = frame_system.get_passes().images.post_images.bright_pixels.clone();
        let sorting_fb = frame_system.get_passes_mut().get_framebuff_simple(target_image);
        let clearing = vec![[0.0,0.0,0.0,0.0].into()];
        let mut local_cb = command_buffer.begin_render_pass(
            sorting_fb, false, clearing
        ).expect("failed to start sorting renderpass!");

        let bloom_brightness = {
            let settings_lck = self.engine_settings.read().expect("failed to lock settings");
            settings_lck.get_render_settings().get_bloom().brightness
        };


        let lum_buf = physical_camera.average_buffer.clone();

        let source_image = frame_system.get_passes().get_diffuse_image();
        let desc = self.sorting_desc_pool.next()
        .add_sampled_image(
            source_image, self.screen_sampler.clone()
        )
        .expect("failed to source image to bright pixel sorting desc")
        .add_buffer(lum_buf)
        .expect("failed to add bright pass settings to desc")
        .build().expect("failed to build bright pass desc");

        //Finally draw screen quad with settings
        local_cb = local_cb.draw(
            self.sorting_pipe.get_pipeline_ref(),
            frame_system.get_dynamic_state(),
            vec![self.screen_vertex_buffer.clone()],
            desc,
            ()
        ).expect("failed to add draw call for the bright pixel plane");

        local_cb = local_cb.end_render_pass().expect("failed to end sorting pass");

        //Now blit down the layers
        let mut source: Arc<ImageAccess + Send + Sync + 'static> = frame_system.get_passes()
        .images.post_images.bright_pixels.clone();

        for hdr_level in frame_system.get_passes().images.post_images.scaled_hdr.iter(){
            local_cb = post_process::blit_images(
                local_cb,
                source.clone(),
                hdr_level.input_image.clone()
            );
            //Overwrite with new source
            source = hdr_level.input_image.clone();
        }
        local_cb
    }

    fn blur(
        &mut self,
        command_buffer: AutoCommandBufferBuilder,
        frame_system: &FrameSystem,
        sampler: Arc<Sampler>,
    ) -> AutoCommandBufferBuilder{

        let mut local_cb = command_buffer;

        //Blur each level, this time we start with the last and add them up until we reached the last
        //image
        let num_blur_levels = frame_system
        .get_passes().images.post_images.scaled_hdr.len();
        //Find the first blur level. We usually don't want to blur the nearly full hd texture first
        let initial_blur_level = {
            self.engine_settings
            .read().expect("failed to lock settings")
            .get_render_settings().get_bloom().first_bloom_level as usize
        };


        let mut is_first_img = true;
        for idx in (initial_blur_level..num_blur_levels).rev(){

            let target_stack = frame_system.get_passes()
            .images.post_images.scaled_hdr[idx].clone();
            //We only want to add a previouse stack if we are not the lowest image
            let mut previouse_stack = None;
            if !is_first_img{
                previouse_stack = Some(
                    frame_system.get_passes()
                    .images.post_images.scaled_hdr[idx + 1].clone()
                );
            }else{
                is_first_img = false;
            }

            local_cb = self.blur_comp(
                local_cb,
                sampler.clone(),
                target_stack,
                previouse_stack,
            );

        }
        local_cb
    }

    ///Blures a source, image to a target image via a compute shader. Can also add a optional
    /// second image on top of the resulting image.
    fn blur_comp(
        &mut self,
        command_buffer: AutoCommandBufferBuilder,
        sampler: Arc<Sampler>,
        target_stack: BlurStage,
        previouse_stack: Option<BlurStage>,
    )-> AutoCommandBufferBuilder{

        let blur_size = {
            self.engine_settings
            .read().expect("failed to lock settings.")
            .get_render_settings().get_bloom().size
        };

        //Construct the new shader descriptor
        let settings_hori = blur_cmp_shader::ty::blur_settings{
            is_horizontal: 1,
            add_second: 0,
            blur_size: blur_size
        };

        let horizontal_pass_data = self.blur_settings_pool
        .next(settings_hori).expect("failed to allocate new blur settings data.");

        //Fetch images
        let source_image = target_stack.input_image.clone();
        let target = target_stack.after_h_img.clone();
        //Get dimensions for the compute shader
        let dimens = get_dimensions_simple(&source_image);
        //Create descriptor
        let new_desc = self.blur_descset_pool.next()
        .add_sampled_image(source_image.clone(), sampler.clone())
        .expect("failed to add first image to blur shader")
        .add_sampled_image(source_image, sampler.clone()) //Same since we don't want to add
        .expect("failed to add sampled source image")
        .add_buffer(horizontal_pass_data)
        .expect("Failed to add settings buffer")
        .add_image(target) //image to be written
        .expect("failed to add target for blur h pass")
        .build()
        .expect("failed to build blur compute descriptor");

        let dispatch_dims = [
        (dimens[0] / BLOOMTHREADSIZE[0]) + 1,
        (dimens[1] / BLOOMTHREADSIZE[1]) + 1,
         1];

        //now execute
        let mut new_cb = command_buffer.dispatch(dispatch_dims, self.blur_comp_pipe.clone(), new_desc, ())
        .expect("failed to start compute shader");
//================================================================
//Second pass, blur v and might add

        //Fetch images
        let vert_source_image = target_stack.after_h_img.clone();
        let vert_target = target_stack.final_image.clone();

        //Find the right setting for the add_second property
        let (add_second_int, second_img) = if let Some(second_img) = previouse_stack{
            (1, second_img.final_image.clone()) //should add second image
        }else{
            (0, vert_source_image.clone()) //should not add
        };

        let settings_vert = blur_cmp_shader::ty::blur_settings{
            is_horizontal: 0,
            add_second: add_second_int,
            blur_size: blur_size,
        };

        let vertical_pass_data = self.blur_settings_pool
        .next(settings_vert).expect("failed to allocate new blur settings data.");

        //Create descriptor
        let new_desc_vert = self.blur_descset_pool.next()
        .add_sampled_image(vert_source_image.clone(), sampler.clone())
        .expect("failed to add first image to blur shader")
        .add_sampled_image(second_img, sampler.clone()) //might be a second image
        .expect("failed to add sampled source image")
        .add_buffer(vertical_pass_data)
        .expect("Failed to add settings buffer")
        .add_image(vert_target) //image to be written
        .expect("failed to add target for blur h pass")
        .build()
        .expect("failed to build blur compute descriptor");


        //now execute, dims are the same
        new_cb = new_cb.dispatch(dispatch_dims, self.blur_comp_pipe.clone(), new_desc_vert, ())
        .expect("failed to start compute shader");

        new_cb

    }

}

pub fn get_dimensions_simple(image: &ImageAccess) -> [u32; 2]{
    let target_dim = ImageAccess::dimensions(image);
    //Now same for the target
    let target_lower_right = {
        match target_dim{
            ImageDimensions::Dim2d{width, height, array_layers, cubemap_compatible} => [width as u32, height as u32],
            _ => {
                println!("Faking image destination", );
                [1,1]
            }
        }
    };

    target_lower_right
}

///The compute shader used to compute the current average lumiosity of this image
pub mod blur_cmp_shader{
    vulkano_shaders::shader!{
        ty: "compute",
        path: "data/shader/blur_cmp.comp"
    }
}
