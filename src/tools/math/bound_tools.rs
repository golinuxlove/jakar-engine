use cgmath::*;
use collision::*;

use core::next_tree::get_min_max;

pub trait BoundingTransforms {
    ///Transforms a bound and returns the smallest AA-boundingbox covering the resulting Box
    fn transform_bound(&self, transform: Matrix4<f32>) -> Self;
}

impl BoundingTransforms for Aabb3<f32>{
    fn transform_bound(&self, transform: Matrix4<f32>) -> Self{
        let mut points = self.to_corners().to_vec();
        points = points.into_iter().map(|p| {
            transform.transform_point(p)
        }).collect();
        let (min, max) = get_min_max(points);
        Aabb3::new(min, max)
    }
}
