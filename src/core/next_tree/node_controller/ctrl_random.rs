use std::time::Instant;
use cgmath::*;
use collision::*;

use rand::{thread_rng, Rng};

use jakar_tree::node::{NodeController, Node};
use core::next_tree::content::ContentType;
use core::next_tree::jobs::SceneJobs;
use core::next_tree::attributes::NodeAttributes;
use tools::math::time_tools::*;

pub struct RandomMovementController{
    speed: f32,
    interval: Aabb3<f32>,
    direction: Vector3<f32>,
    //The direction we at some point want to reach.
    new_target_dir: Vector3<f32>,
    //How hard the turn to the new target is.
    turn_intensity: f32,
    //How long alon the current direction we have to stay within the bound without changing direction.
    foresee: f32,
    last_update: Instant,
}

impl RandomMovementController{
    ///Creates a default controller which moves within [-1,-1,-1] - [1,1,1] with a speed one 1
    pub fn default() -> Self{
        RandomMovementController{
            speed: 1.0,
            interval: Aabb3::new(Point3::new(-1.0,-1.0,-1.0), Point3::new(1.0,1.0,1.0)),
            direction: Vector3::new(1.0, 0.0, 0.0),
            new_target_dir: Vector3::new(1.0, 0.0, 0.0),
            turn_intensity: 0.5,
            foresee: 2.0,
            last_update: Instant::now(),
        }
    }

    pub fn set_speed(&mut self, new: f32){
        self.speed = new;
    }

    pub fn set_interval(&mut self, new: [Point3<f32>; 2]){
        assert!(new[0].x < new[1].x && new[0].y < new[1].y && new[0].z < new[1].z);
        self.interval = Aabb3::new(new[0], new[1]);
    }

    pub fn set_direction(&mut self, new: Vector3<f32>){
        self.direction = new.normalize();
    }

    ///Controlles how "Hard" the turns are and at which point they will occure.
    pub fn set_turn_intensity_and_foresee(&mut self, turn_int: f32, foresee: f32){
        self.turn_intensity = turn_int;
        self.foresee = foresee;
    }
}

impl NodeController<ContentType,SceneJobs,NodeAttributes> for RandomMovementController
{
    fn update(&mut self, node: &mut Node<ContentType,SceneJobs,NodeAttributes>){

        let current_location = node.get_attrib().transform.disp;
        let delta = dur_as_f32(self.last_update.elapsed());
        self.last_update = Instant::now();
        //Check if we can continue along this way
        let can_continue = self.interval.contains(&Sphere{center: Point3::from_vec(current_location), radius: 0.001});

        if can_continue{
            //Correct way based on target direction
            self.direction = self.direction.lerp(self.new_target_dir, self.turn_intensity);
            node.add_job(SceneJobs::Move(self.direction * self.speed * delta));
            return;
        }
        //We have to correct our way
        //Check if we reach the box if we travel along target direction
        //if so, we just adjust the direction slightly, otherwise we choose a random point within our box
        //and update target direction
        if self.interval.intersects(&Ray3::new(Point3::from_vec(current_location), self.new_target_dir)){
            self.direction = self.direction.lerp(self.new_target_dir, self.turn_intensity);
            node.add_job(SceneJobs::Move(self.direction * self.speed * delta));
        }else{
            let mut rng = thread_rng();
            let x = rng.gen_range(self.interval.min.x, self.interval.max.x);
            let y = rng.gen_range(self.interval.min.y, self.interval.max.y);
            let z = rng.gen_range(self.interval.min.z, self.interval.max.z);
            //Update target dir to point to a point within the volume
            self.new_target_dir = Vector3::new(x,y,z) - current_location;
            self.direction = self.direction.lerp(self.new_target_dir, self.turn_intensity);
            node.add_job(SceneJobs::Move(self.direction * self.speed * delta));
        }
    }
}
