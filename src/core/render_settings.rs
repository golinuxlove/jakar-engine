


///Can be used to change between variouse debug views. They are actually applied in the
///post progress stage
#[derive(Clone)]
pub enum DebugView {
    MainDepth,
    DebugGrid,
    ShadowMaps,
    DirectionalDepth,
    GBuffer,
    GBufferTwo,
    Shaded,
}

impl DebugView{
    pub fn as_shader_int(&self) -> i32{
        match self{
            &DebugView::MainDepth => 0,
            &DebugView::DebugGrid => 1,
            &DebugView::ShadowMaps => 2,
            &DebugView::DirectionalDepth => 3,
            &DebugView::GBuffer => 4,
            &DebugView::GBufferTwo => 5,
            &DebugView::Shaded => 6,
        }
    }
}

///Describes several settings which are used while debugging.
#[derive(Clone)]
pub struct DebugSettings {
    ///Can be used to switch between debug views.
    pub debug_view: DebugView,
    ///Describes which level of the scaled ldr image should be shown when in
    pub ldr_debug_view_level: u32,
    ///Is true if you want to let the engine draw the bounds of the objects
    pub draw_bounds: bool,
}

///BloomSettings.
#[derive(Clone)]
pub struct BloomSettings {
    ///more levels means nicer bloom but worse performance1
    pub levels: u32,
    ///Describes what size the initial blur image should be scaled down.
    pub first_bloom_level: u32,
    ///How big the bloom should be. This value should mostly be below 1.0.
    pub size: f32,
    ///How bright the added blure samples are. 1.0 is realy bright. *The values should be between 1.0 and 0.0*
    pub brightness: f32
}

impl BloomSettings{
    pub fn new(levels: u32, scale_down: u32, size: f32, brightness: f32) -> Self{
        BloomSettings{
            levels,
            first_bloom_level: scale_down,
            size,
            brightness
        }
    }
}

///Collects all settings which the ssao can have
#[derive(Clone)]
pub struct SsaoSettings {
    ///How many samples should be taken for the ssao
    pub sample_count: u32,
    ///The size around a pixel which should be considered for ssao
    pub kernel_radius: f32,
    ///The actuall intensity which is added/substracted to the image
    pub intensity: f32,
}

impl SsaoSettings{
    pub fn new(sample_count: u32, kernel_radius: f32, intensity: f32) -> Self{
        SsaoSettings{
            sample_count,
            kernel_radius,
            intensity
        }
    }

    ///A set of default settings which work good:
    /// - sample_count: 11
    /// - kernel_radius: 0.5
    /// - intensity: 0.95
    pub fn default() -> Self{
        SsaoSettings{
            sample_count: 11,
            kernel_radius: 0.5,
            intensity: 0.95
        }
    }
}

///Describes the properties used when rendering the shadows
#[derive(Clone)]
pub struct ShadowSettings {
    /// i used for finding the shadow map resolution by doing `vec2(width, height) / pass_resolution`
    ///where eidth and height are the dimensions of the rendered image.
    pub pass_resolution: usize,
    ///Defines how many lights can potentually cast shadows
    pub max_casting_lights: usize,
}

impl ShadowSettings{
    pub fn default() -> Self{
        ShadowSettings{
            pass_resolution: 2,
            max_casting_lights: 3,
        }
    }

    pub fn new(resolution_division: usize, max_shadow_lights: usize) -> Self{
        ShadowSettings{
            pass_resolution: resolution_division,
            max_casting_lights: max_shadow_lights,
        }
    }
}

///Defines several settings which will be used to determin how lights and their shadows are rendered
#[derive(Clone)]
pub struct LightSettings {
    pub shadow_settings: ShadowSettings
}

impl LightSettings{
    ///Creates a custom set of light settings
    pub fn new(shadow_settings: ShadowSettings) -> Self{
        LightSettings{
            shadow_settings: shadow_settings,
        }
    }

    ///Creates the default settings, see the impls of the different settings to see them.
    pub fn default() -> Self{
        LightSettings{
            shadow_settings: ShadowSettings::default(),
        }
    }
}

///Descibes settings the renderer can have. Most of the values can't be changed after
/// starting the engine.
///Things to keep in mind:
///
/// - if you turn hdr off you won't have any bloom effects as well.
#[derive(Clone)]
pub struct RenderSettings {
    ///Samples for each pixel, should be power of two between 1 and 16 (but can be higher)
    msaa: u32,
    ///Is true if a "fifo" presentmode of the swapchain should be forced.
    v_sync: bool,

    ///Defines the current gamma correction on the output
    gamma: f32,

    ///Collects all settings related to light and shadows
    light_settings: LightSettings,

    ///Defines the bloom settings. Mainly strength and scale.
    bloom: BloomSettings,

    ///Defines all ssao related settings
    ssao: SsaoSettings,

    ///Describes the several debug settings one cna change
    debug_settings: DebugSettings,

}


impl RenderSettings{
    ///Creates a default, medium good set of render settings:
    /// Defaults:
    ///
    /// - msaa: 1,
    /// - v_sync: false,
    /// - gamma: 2.2,
    /// - exposure: 1.0
    /// - max_point_lights: 512,
    /// - max_dir_lights: 6,
    /// - max_spot_lights: 512,

    /// *No debug turned on*
    pub fn default() -> Self{
        RenderSettings{
            msaa: 2,
            v_sync: false,
            gamma: 2.2,
            light_settings: LightSettings::default(),

            bloom: BloomSettings{
                levels: 8,
                first_bloom_level: 4, //nice performance and look
                size: 0.05,
                brightness: 1.0,
            },

            ssao: SsaoSettings::default(),

            debug_settings: DebugSettings{
                draw_bounds: false,
                debug_view: DebugView::Shaded,
                ldr_debug_view_level: 0,
            }
        }
    }

    ///Returns the current debug settings as mutable.
    #[inline]
    pub fn get_debug_settings(&self) -> &DebugSettings{
        &self.debug_settings
    }

    ///Returns the current debug settings as mutable.
    #[inline]
    pub fn get_debug_settings_mut(&mut self) -> &mut DebugSettings{
        &mut self.debug_settings
    }

    ///sets the view type to be used for the following frames
    #[inline]
    pub fn set_debug_settings(&mut self, new: DebugSettings){
        self.debug_settings = new;
    }


    ///Sets up a custom anisotropical filtering factor, should be a power of two as well, otherwise
    /// it won't be used
    #[inline]
    pub fn with_msaa_factor(mut self, msaa_factor: u32) -> Self{
        if !test_for_power_of_two(msaa_factor) || msaa_factor > 16 || msaa_factor < 2{ //TODO allow no multisampling
            return self;
        }
        self.msaa = msaa_factor;
        self
    }
    ///Returns the current msaa factor. Is always a power of two.
    #[inline]
    pub fn get_msaa_factor(&self) -> u32{
        self.msaa
    }

    ///set the v_sync falue to either true or false. However the engine will check if
    /// Vulkans Immidiate present mode is supported, if v_sync is turned of. If it is not, V_Sync
    /// will be used (always supported).
    #[inline]
    pub fn with_vsync(mut self, value: bool) -> Self{
        self.v_sync = value;
        self
    }


    ///Sets the vsync mode at runtime. However this won't have an effect if the renderer has already
    /// been started.
    pub fn set_vsync(&mut self, value: bool){
        self.v_sync = value;
    }

    ///Returns the current v_sync status. Will be changed to false at runtime if non-vsync presenting
    /// is not supported.
    #[inline]
    pub fn get_vsync(&self) -> bool{
        self.v_sync
    }

    ///Sets up a custom gamma value.
    #[inline]
    pub fn with_gamma(mut self, gamma: f32) -> Self{
        self.gamma = gamma;
        self
    }
    ///Returns the current gamma factor.
    #[inline]
    pub fn get_gamma(&self) -> f32{
        self.gamma
    }

    ///Sets the current gamma factor to `new` and marks the settings as unresolved.
    #[inline]
    pub fn set_gamma(&mut self, new: f32){
        self.gamma = new;
    }

    ///Adds an ammount to gamma. NOTE: the gamma can't be below 0.0 all values below will
    /// be clamped to 0.0
    #[inline]
    pub fn add_gamma(&mut self, offset: f32){

        if self.gamma - offset >= 0.0{
            self.gamma -= offset;
        }else{
            self.gamma = 0.0
        }
    }

    ///Returns the current light settings as a clone.
    #[inline]
    pub fn get_light_settings(&self) -> LightSettings{
        self.light_settings.clone()
    }

    ///Returns the current light settings as mutable reference.
    #[inline]
    pub fn get_light_settings_mut(&mut self) -> &mut LightSettings{
        &mut self.light_settings
    }

    ///Sets the current light settings
    #[inline]
    pub fn set_light_settings(&mut self, new: LightSettings){
        self.light_settings = new;
    }

    ///Sets the current light settings when building the rendering settings
    #[inline]
    pub fn with_light_settings(mut self, new: LightSettings) -> Self{
        self.light_settings = new;
        self
    }

    ///Sets the current bloom settings. Don't overdo it or your rendered image will look like a Michael Bay movie.
    ///Also for performance, try to increase the initial scale down
    #[inline]
    pub fn with_bloom(mut self, levels: u32, first_bloom_level: u32, size: f32, brightness: f32) -> Self{
        self.set_bloom(levels, first_bloom_level, size, brightness);
        self
    }

    ///Sets the current bloom settings. Don't overdo it or your rendered image will look like a Michael Bay movie.
    #[inline]
    pub fn set_bloom(&mut self, levels: u32, first_bloom_level: u32, size: f32, brightness: f32){
        let mut using_first_level = first_bloom_level;
        if first_bloom_level >= levels{
            using_first_level = levels - 1;
        }

        let mut used_brightness = brightness;
        if brightness > 1.0{
            used_brightness = 1.0;
        }
        if brightness < 0.0 {
            used_brightness = 0.0
        }
        self.bloom = BloomSettings::new(levels, using_first_level, size, used_brightness);
    }

    ///Returns the current bloom settings. They might change per frame.
    #[inline]
    pub fn get_bloom(&self) -> BloomSettings{
        self.bloom.clone()
    }

    ///Returns the current bloom settings. They might change per frame.
    #[inline]
    pub fn get_bloom_mut(&mut self) -> &mut BloomSettings{
        &mut self.bloom
    }


    ///Creats the settings with thoose values
    #[inline]
    pub fn with_ssao(mut self, sample_count: u32, radius: f32, intensity: f32) -> Self{
        self.set_ssao(sample_count, radius, intensity);
        self
    }

    ///Sets the current Ssao settings.
    #[inline]
    pub fn set_ssao(&mut self, sample_count: u32, radius: f32, intensity: f32){

        let mut using_intensity = intensity;
        if using_intensity < 0.0{
            using_intensity = 0.0;
        }

        let mut used_radius = radius;
        if used_radius <= 0.0{
            used_radius = 0.0001;
        }

        let mut used_sample_count = sample_count;

        self.ssao = SsaoSettings::new(used_sample_count, used_radius, using_intensity);
    }

    ///Returns the current SSAO settings. They might change per frame.
    #[inline]
    pub fn get_ssao(&self) -> SsaoSettings{
        self.ssao.clone()
    }

    ///Returns the current SSAO settings. They might change per frame.
    #[inline]
    pub fn get_ssao_mut(&mut self) -> &mut SsaoSettings{
        &mut self.ssao
    }

}

///Tests for power of two
fn test_for_power_of_two(num: u32) -> bool{
    let mut walker: i32 = num as i32;
    //test if we can dev by two and don't get a remainder and if we are > 1
    while walker > 1 && (walker % 2 == 0){
        walker /= 2;
    }
    //if the last walker was 1 it is a power of two otherwise we got something like 0.5 or 1.5;
    (walker == 1)
}
