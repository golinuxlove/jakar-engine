//TODO Add command buffer creation per mesh
use render::pipeline_manager::PipelineManager;
use render::uniform_manager::UniformManager;
use vulkano::command_buffer::DynamicState;
use std::sync::{Arc, RwLock};
use std::time::Instant;
use cgmath::*;
use collision::*;



use render::frame_system::{FrameSystem};
use render::renderer::RenderDebug;
use render::shader::shader_inputs::ray_tracing;


use vulkano::buffer::ImmutableBuffer;
use vulkano::device::Device;
use vulkano::buffer::BufferUsage;
use vulkano::buffer::BufferAccess;
use vulkano::device::Queue;
use vulkano::command_buffer::AutoCommandBufferBuilder;

use core::ReturnBoundInfo;
use core::resources::material;

use core::resources::bvh_builder::*;
use tools::math::time_tools::as_ms;
use tools::vec_tools::*;
///Defines the information a Vertex should have
#[derive(Clone,Copy)]
pub struct Vertex {
    position: [f32; 3],
    tex_coord: [f32; 2],
    normal: [f32; 3],
    tangent: [f32; 4],
    color: [f32; 4],
}

//Implements the vulkano::vertex trait on Vertex
impl_vertex!(Vertex, position, tex_coord, normal, tangent, color);

//TODO
//Every mesh needs its own indice and vertex buffer plus its pipeline to be drawn
impl Vertex{
    ///Creates a new Vertex
    pub fn new(
        position: [f32; 3],
        tex_coord: [f32; 2],
        normal: [f32; 3],
        tangent: [f32; 4],
        color: [f32; 4]
        ) -> Self
    {
        Vertex{
            position: position,
            tex_coord: tex_coord,
            normal: normal,
            tangent: tangent,
            color: color,
        }
    }
}

///Defines a mesh, a mesh mostly consists of: Name, Vertices (and the corresbondig vertex buffer)
///, the vertex indices, a material and its AABB (bounding box)
#[derive(Clone)]
pub struct Mesh {
    pub name: String,
    device: Arc<Device>,

    ///Holds the raw vertices of this mesh
    vertices: Vec<Vertex>,
    vertex_buffer: Arc<ImmutableBuffer<[Vertex]>>,
    ///Indices used to correctly draw the vertexes
    indices: Vec<u32>,
    index_buffer: Arc<ImmutableBuffer<[u32]>>,

    //one bottom level bvh node, each leaf is a face in the face buffer.
    mesh_bvh: Arc<Vec<ray_tracing::ty::BvhNode>>,
    //Faces of this mesh
    faces: Arc<Vec<ray_tracing::ty::Face>>,
    //Vertices which make up the faces of that mesh
    vert: Arc<Vec<ray_tracing::ty::Vertex>>,
    //tree, which can also be ray traced on the GPU
    bvh_tree: Arc<BvhNode>,

    material: Arc<RwLock<material::Material>>,

    bound: Aabb3<f32>,
}

impl Mesh {
    ///Returns the Member with the passed `name` TODO: Return the upload future for efficient import.
    pub fn new(
        name: &str,
        device: Arc<Device>,
        material: Arc<RwLock<material::Material>>,
        vertices: Vec<Vertex>,
        indices: Vec<u32>,
        upload_queue: Arc<Queue>
    )
        ->Self{
        //Creating the box extend from the location, there might be a better way
        let min = Point3::new(0.5, 0.5, 0.5);
        let max = Point3::new(0.5, 0.5, 0.5);

        //create both buffers and wait for the graphics card to actually upload them
        let (vertex_buffer, _buffer_future) = ImmutableBuffer::from_iter(
            vertices.iter().cloned(),
            BufferUsage::all(),
            upload_queue.clone()
        ).expect("failed to create vertex buffer");


        let (index_buffer, _future) = ImmutableBuffer::from_iter(
            indices.iter().cloned(),
            BufferUsage::all(),
            upload_queue.clone()
        ).expect("failed to create index buffer");

        let (mesh_bvh, faces, vert, bvh_tree) = triangle_to_bvh(&vertices, &indices);

        Mesh{
            name: String::from(name),

            device: device.clone(),

            //TODO Create a persistend vertex and indice buffer
            vertices: vertices,
            vertex_buffer: vertex_buffer,
            indices: indices,
            index_buffer: index_buffer,

            mesh_bvh: Arc::new(mesh_bvh),
            faces: Arc::new(faces),
            vert: Arc::new(vert),
            bvh_tree,

            material: material,

            bound: Aabb3::new(min, max),
        }
    }

    /// Returns how many trinagles are used to draw this objects.
    pub fn get_triangle_count(&self) -> usize{
        self.indices.len() / 3
    }

    ///Appends the this mesh to the several buffer. Saves itselfs mesh data (offsets into buffer
    /// and transform info) into the mesh buffer.
    pub fn as_ray_vertex_buffer(
        &self,
        transform: Matrix4<f32>,
        mesh_buffer: &mut Vec<ray_tracing::ty::Mesh>,
        bottom_bvh_buffer: &mut SubVec<ray_tracing::ty::BvhNode>,
        faces_buffer: &mut SubVec<ray_tracing::ty::Face>,
        vertex_buffer: &mut SubVec<ray_tracing::ty::Vertex>,
        //debug: &mut TimeDebug,
    ){
        //let this_mesh_idx = mesh_buffer.len();
        let bottom_bvh_offset = bottom_bvh_buffer.len();
        let bottom_bvh_length = self.mesh_bvh.len();
        let faces_buffer_offset = faces_buffer.len();
        let vertex_buffer_offset = vertex_buffer.len();

        bottom_bvh_buffer.push(self.mesh_bvh.clone());
        faces_buffer.push(self.faces.clone());
        vertex_buffer.push(self.vert.clone());

        mesh_buffer.push(
            ray_tracing::ty::Mesh{
                modelMatrix: transform.into(),
                invModelMatrix: transform.invert().expect("failed to invert matrix").into(),
                bottom_bvh_offset: bottom_bvh_offset as u32,
                bottom_bvh_length: bottom_bvh_length as u32,
                face_offset: faces_buffer_offset as u32,
                vert_offset: vertex_buffer_offset as u32,
            }
        );
    }

    ///Returns the name of the material this mesh uses
    #[inline]
    pub fn get_material_name(&self) -> String{
        self.material.read().expect("failed to lock meshs material").get_name()
    }

    ///Returns the material in use by this mesh as clone
    #[inline]
    pub fn get_material(&self) -> Arc<RwLock<material::Material>>{
        self.material.clone()
    }

    ///Can be used to set the mesh's material to a new one
    #[inline]
    pub fn set_material(&mut self, new_mat: Arc<RwLock<material::Material>>){
        self.material = new_mat;
    }

    ///Returns all indices
    #[inline]
    pub fn get_indices(&self) -> Vec<u32>{
        self.indices.clone()
    }

    ///Return all vertices
    #[inline]
    pub fn get_all_vertices(&self) -> Vec<Vertex>{
        self.vertices.clone()
    }

    ///Returns all pos data
    #[inline]
    pub fn get_all_positions(&self)-> Vec<[f32; 3]>{
        let mut return_vec = Vec::new();
        for i in self.vertices.iter(){
            return_vec.push(i.position);
        }
        return_vec
    }

    ///Returns all pos data
    #[inline]
    pub fn get_all_uvs(&self)-> Vec<[f32; 2]>{
        let mut return_vec = Vec::new();
        for i in self.vertices.iter(){
            return_vec.push(i.tex_coord);
        }
        return_vec
    }

    ///Returns all pos data
    pub fn get_all_normals(&self)-> Vec<[f32; 3]>{
        let mut return_vec = Vec::new();
        for i in self.vertices.iter(){
            return_vec.push(i.normal);
        }
        return_vec
    }

    ///Returns all pos data
    pub fn get_all_tangents(&self)-> Vec<[f32; 4]>{
        let mut return_vec = Vec::new();
        for i in self.vertices.iter(){
            return_vec.push(i.tangent);
        }
        return_vec
    }

    ///Returns all pos data
    pub fn get_all_colors(&self)-> Vec<[f32; 4]>{
        let mut return_vec = Vec::new();
        for i in self.vertices.iter(){
            return_vec.push(i.color);
        }
        return_vec
    }

    ///Returns the current vertex buffer of this mesh
    pub fn get_vertex_buffer(&self) -> Arc<BufferAccess + Send + Sync>{
        self.vertex_buffer.clone()
    }

    ///Recreates the vertex buffer from a specified device and queue
    pub fn re_create_buffer(&mut self, upload_queue: Arc<Queue>)
    {
        //create both buffers and wait for the graphics card to actually upload them
        let (vertex_buffer, _buffer_future) = ImmutableBuffer::from_iter(
            self.vertices.iter().cloned(),
            BufferUsage::all(),
            upload_queue.clone()
        ).expect("failed to create vertex buffer");


        let (index_buffer, _future) = ImmutableBuffer::from_iter(
            self.indices.iter().cloned(),
            BufferUsage::all(),
            upload_queue.clone()
        ).expect("failed to create index buffer");

        //overwrite internally
        self.vertex_buffer = vertex_buffer;
        self.index_buffer = index_buffer;
    }

    ///Returns a index bufffer for this mesh
    pub fn get_index_buffer(&self) -> Arc<ImmutableBuffer<[u32]>>
    {
        self.index_buffer.clone()
    }

    ///Renders this mesh if the supplied framestage is in the froward stage
    pub fn draw(
        &self,
        command_buffer: AutoCommandBufferBuilder,
        frame_system: &FrameSystem,
        transform: Matrix4<f32>,
        debug: &mut RenderDebug,
    ) -> AutoCommandBufferBuilder{

        let material_locked = self.get_material();
        let mut material = material_locked
        .write()
        .expect("failed to lock mesh for command buffer generation");

        let pipeline = material.get_vulkano_pipeline();

        debug.start_set_gen();

        let set_01 = {
            //aquirre the tranform matrix and generate the new set_01
            material.get_set_01(transform)
        };

        let set_02 = {
            material.get_set_02()
        };

        let set_03 = {
            material.get_set_03()
        };


        debug.end_mesh_set();
        debug.start_draw_cmd();

        //extend the current command buffer by this mesh
        let new_cb = command_buffer.draw_indexed(
            pipeline,
            frame_system.get_dynamic_state(),
            vec![self.get_vertex_buffer()], //TODO Group by material?
            self.get_index_buffer(),
            (set_01, set_02, set_03), //descriptor sets (currently static)
            ()
        )
        .expect("Failed to draw mesh in command buffer!");

        debug.end_draw_cmd();

        new_cb
    }

    pub fn as_mesh_data(&self) -> MeshDataSize{
        MeshDataSize{
            bottom_bvh_buffer_len: self.mesh_bvh.len(),
            mesh_buffer_len: 1,
            faces_buffer: self.faces.len(),
            vertex_buffer: self.vert.len(),
        }
    }


    pub fn draw_bvh(&self,
        command_buffer: AutoCommandBufferBuilder,
        pipeline_manager: Arc<RwLock<PipelineManager>>,
        device: Arc<Device>,
        uniform_manager: Arc<RwLock<UniformManager>>,
        dynamic_state: &DynamicState,
        transform: Option<Matrix4<f32>>
    ) -> AutoCommandBufferBuilder{
        self.bvh_tree.draw_bound(
            1, //lvl 1 / for no DIV by 0
            command_buffer,
            pipeline_manager,
            device,
            uniform_manager,
            dynamic_state,
            transform
        )

    }

}

impl ReturnBoundInfo for Mesh{
    ///return the max size of its bound
    #[inline]
    fn get_bound_max(&self)-> Point3<f32>{
        self.bound.max.clone()
    }
    ///return the min size of its bound
    #[inline]
    fn get_bound_min(&self)-> Point3<f32>{
        self.bound.min.clone()
    }
    ///Sets the bound to the new values (in mesh space)
    #[inline]
    fn set_bound(&mut self, min: Point3<f32>, max: Point3<f32>){
        self.bound = Aabb3::new(min, max);
    }

    ///Returns it' bound
    #[inline]
    fn get_bound(&self) -> Aabb3<f32>{
        self.bound.clone()
    }


    ///Returns the vertices of the bounding mesh, good for debuging
    fn get_bound_points(&self)-> Vec<Vector3<f32>>{
        let mut return_vector = Vec::new();

        let b_min = self.bound.min.clone();
        let b_max = self.bound.max.clone();

        //low
        return_vector.push(Vector3::new(b_min[0], b_min[1], b_min[2])); //Low
        return_vector.push(Vector3::new(b_min[0] + b_max[0], b_min[1], b_min[2])); //+x
        return_vector.push(Vector3::new(b_min[0], b_min[1] + b_max[1], b_min[2])); //+y
        return_vector.push(Vector3::new(b_min[0],  b_min[1], b_min[2] + b_max[2])); // +z
        return_vector.push(Vector3::new(b_min[0] + b_max[0], b_min[1] + b_max[1], b_min[2])); //+xy
        return_vector.push(Vector3::new(b_min[0] + b_max[0], b_min[1], b_min[2] + b_max[2])); //+xz
        return_vector.push(Vector3::new(b_min[0] , b_min[1] + b_max[1], b_min[2] + b_max[1])); //+yz
        return_vector.push(Vector3::new(b_min[0] + b_max[0], b_min[1] + b_max[1], b_min[2] + b_max[2])); //+xyz

        return_vector
    }

    ///Rebuilds nothing, but might be able in the future to actually rebuild the bound based on all of the vertexes
    fn rebuild_bound(&mut self){
        let (mins, maxs) = {
            //Set min and max to a possible position
            let mut min: [f32; 3] = self.vertices[0].position;
            let mut max: [f32; 3] = self.vertices[0].position;

            for position in self.get_all_positions(){
                //X val
                //min
                if position[0] < min[0]{
                    min[0] = position[0];
                }
                //max
                if position[0] > max[0]{
                    max[0] = position[0];
                }


                //Y val
                //min
                if position[1] < min[1]{
                    min[1] = position[1];
                }
                //max
                if position[1] > max[1]{
                    max[1] = position[1];
                }
                //Z val
                //min
                if position[2] < min[2]{
                    min[2] = position[2];
                }
                //max
                if position[2] > max[2]{
                    max[2] = position[2];
                }
            }

            (Point3::new(min[0], min[1], min[2]), Point3::new(max[0], max[1], max[2]))
        };

        self.set_bound(mins, maxs);
    }
}

struct SelfContainedFace{
    vert0: ray_tracing::ty::Vertex,
    vert1: ray_tracing::ty::Vertex,
    vert2: ray_tracing::ty::Vertex,
}

impl BvhBuildable for SelfContainedFace{
    fn get_center<T: BvhBuildable>(&self, buffer: &Vec<T>) -> Point3<f32>{
        let x = self.vert0.pos[0] +
            self.vert1.pos[0] +
            self.vert2.pos[0];

        let y = self.vert0.pos[1] +
            self.vert1.pos[1] +
            self.vert2.pos[1];

        let z = self.vert0.pos[2] +
            self.vert1.pos[2] +
            self.vert2.pos[2];

        Point3::new(x,y,z) / 3.0
    }
    fn get_aabb<T: BvhBuildable>(&self, buffer: &Vec<T>) -> Aabb3<f32>{
        let min = Point3::new(
            self.vert0.pos[0].min(self.vert1.pos[0].min(self.vert2.pos[0])),
            self.vert0.pos[1].min(self.vert1.pos[1].min(self.vert2.pos[1])),
            self.vert0.pos[2].min(self.vert1.pos[2].min(self.vert2.pos[2]))
        );

        let max = Point3::new(
            self.vert0.pos[0].max(self.vert1.pos[0].max(self.vert2.pos[0])),
            self.vert0.pos[1].max(self.vert1.pos[1].max(self.vert2.pos[1])),
            self.vert0.pos[2].max(self.vert1.pos[2].max(self.vert2.pos[2]))
        );

        Aabb3::new(min, max)
    }
    fn get_sah<T: BvhBuildable>(&self, buffer: &Vec<T>) -> f32{
        self.get_aabb(buffer).surface_area()
    }
    fn get_triangle_count<T: BvhBuildable>(&self, buffer: &Vec<T>) -> u32{
        1
    }

    fn get_mesh_data<T: BvhBuildable>(&self, buffer: &Vec<T>) -> MeshDataSize{
        MeshDataSize{
            bottom_bvh_buffer_len: 0,
            mesh_buffer_len: 0,
            faces_buffer: 1,
            vertex_buffer: 3,
        }
    }
}

///Takes a list of triangles and builds a flat bvh from them. Similar to the top level bvh in core::next_tree::mesh_bvh
/// but without the SHA. Returns the bvh nodes as well as the faces which make up the leafs of the bvh and the vertices each face is made of.
/// assumes a triangluated model.
pub fn triangle_to_bvh(
    vertices: &Vec<Vertex>,
    indices: &Vec<u32>,
) -> (Vec<ray_tracing::ty::BvhNode>, Vec<ray_tracing::ty::Face>, Vec<ray_tracing::ty::Vertex>, Arc<BvhNode>){
    //First of all, build the vertex buffer, then compute the a list of faces (aka find the right indice in the buffer).
    //Then compute each faces location. After that assign morton codes and order the faces

    let vertex_buffer: Vec<ray_tracing::ty::Vertex> = vertices.clone().into_iter().map(|vert| ray_tracing::ty::Vertex{
        pos: Vector3::from(vert.position).extend(1.0).into(),
        uv: vert.tex_coord,
        normal: Vector3::from(vert.normal).extend(0.0).into(),
        tangent: Vector4::from(vert.tangent).into(),
        _pad0: 0.0,
        _pad1: 0.0,
    }).collect();


    //Since we did not change the order, the indice buffer should be good. Therefor, create Faces
    let mut faces = Vec::new();
    let mut index_iter = indices.clone().into_iter();
    //TODO use exact_chunks instead
    loop{
        let v0 = if let Some(idx) = index_iter.next(){
            idx
        }else{
            break;
        };
        let v1 = if let Some(idx) = index_iter.next(){
            idx
        }else{
            break;
        };
        let v2 = if let Some(idx) = index_iter.next(){
            idx
        }else{
            break;
        };

        //Currently using the pre transformed verts
/*
        //Transform to points, the transform to world space finaly add
        let p0 = Vector3::from(vertices[v0 as usize].position).extend(1.0);
        let p1 = Vector3::from(vertices[v1 as usize].position).extend(1.0);
        let p2 = Vector3::from(vertices[v2 as usize].position).extend(1.0);


        faces.push(ray_tracing::ty::Face{
            vert0: p0.into(),
            vert1: p1.into(),
            vert2: p2.into(),
        });
*/

        faces.push(ray_tracing::ty::Face{
            vert0: v0,
            vert1: v1,
            vert2: v2,
            _pad0: 0,
        });
    }

    assert_eq!(faces.len() * 3, indices.len(), "Index buffer has different size then face buffer!");

    //Build an intermediate face type, which is used to build the bvh.

    let inter_faces: Vec<SelfContainedFace> = faces.iter().map(|f| SelfContainedFace{
        vert0: vertex_buffer[f.vert0 as usize],
        vert1: vertex_buffer[f.vert1 as usize],
        vert2: vertex_buffer[f.vert2 as usize],
    }).collect();


    //Build the bvh nodes of this faces
    let tree = build_tree(&inter_faces);

    let mut mesh_bvh: Vec<ray_tracing::ty::BvhNode> = tree.to_flat_bottom(
        None,
    );

    //Sort the bvh array to be in order.
    mesh_bvh.sort_unstable_by(
        |node1, node2| node1.idx.cmp(&node2.idx)
    );

    for (idx, node) in mesh_bvh.iter().enumerate(){
        if idx as i32 != node.idx{
            println!("WARNING INVALID BVH INDEX", );
        }
    }

    (mesh_bvh, faces, vertex_buffer, tree)
}
