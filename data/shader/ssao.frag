#version 450

//input depth and normal used for the calculation
layout(set = 0, binding = 0) uniform sampler2D t_Depth;
layout(set = 0, binding = 1) uniform sampler2D t_Normal;
layout(set = 0, binding = 2) uniform sampler2D t_Ambient;
layout(set = 0, binding = 3) uniform ssao_settings{
  mat4 projection;
  mat4 invProjection;
  mat4 view;
  int kernel_size;
  float kernel_radius;
  vec2 near_far;
} u_ssao_settings;

//Get the uvs
layout(location = 0) in vec2 inter_coord;
layout(location = 1) in vec2 v_pos;

layout(location = 0) out float final_ao;


float linear_depth(vec2 uv){
  float depth = texture(t_Depth, uv).r;
  float f= u_ssao_settings.near_far.y;
  float n = u_ssao_settings.near_far.x;
  float z = (2 * n) / (f + n - depth * (f - n));
  return z;
}

vec3 pos_from_depth(vec2 uv){
  float z = texture(t_Depth, uv).r * 2.0 - 1.0;
  vec2 xy = uv * 2.0 - 1.0;
  vec4 projected_pos = vec4(xy, z, 1.0);
  vec4 vPosVS = u_ssao_settings.invProjection * projected_pos;
  vPosVS.xyz /= vPosVS.w;
  //vPosVS.xyz /= vPosVS.w;
  return vPosVS.xyz;

}

vec3 get_normal(vec2 uv){

  vec2 texel_size = 1.0 / textureSize(t_Depth, 0);

  vec2 tang_offset = vec2(0.0, texel_size.y);
  vec3 tang_pos = pos_from_depth(uv + tang_offset);

  vec2 bitang_offset = vec2(texel_size.x, 0.0);
  vec3 bitang_pos = pos_from_depth(uv + bitang_offset);

  vec3 normal = cross(normalize(bitang_pos), normalize(tang_pos));
  //Now do cross
  return normal;
}


//Creates a random float between 0.0 and 1.0
float randomf(vec4 seed){
  float dot_product = dot(seed, vec4(12.9898,78.233,45.164,94.673));
  return fract(sin(dot_product) * 43758.5453);
}

vec3 randomvec3(vec4 seed){
  //DO half a sphere
  vec3 sample_vec = vec3(
    randomf(seed * 0.5) * 2.0 - 1.0,
    randomf(seed * 0.25) * 2.0 - 1.0,
    randomf(seed * 0.4)
    );

    sample_vec = normalize(sample_vec);
    float scale = seed.w / 64.0; //we know that w is the index
    scale = mix(0.1, 1.0, scale * scale);
    sample_vec *= scale;
    return sample_vec;
}



void main(){

  //Early return if the gbuffer comes whith its own ao values at that point
  float gbuffer_ao = texture(t_Ambient, inter_coord).r;
  if ( gbuffer_ao != 1.0 && gbuffer_ao != 0.0){
    //Pre invert the value since in will be inverted in the finalize shader.

    final_ao = 1.0 - texture(t_Ambient, inter_coord).r;
    return;
  }


  vec3 p = pos_from_depth(inter_coord);
  vec3 nrm = texture(t_Normal, inter_coord).rgb;
  vec4 tmp_n = inverse(transpose(u_ssao_settings.view)) * vec4(nrm, 0.0);
  vec3 n = normalize(tmp_n.xyz);
  float lin_depth = linear_depth(inter_coord);

  float ao = 0.0f;
  float rad = u_ssao_settings.kernel_radius *  (1.0 - lin_depth);

  for (int idx=0; idx<u_ssao_settings.kernel_size; idx++){
    //Get a random vector
    vec3 random_vec = randomvec3(vec4(p, float(idx))) * rad + p;
    //Transform into prespective to get uvs for depth
    vec4 pers_pos = u_ssao_settings.projection * vec4(random_vec, 1.0);
    //pers_pos.xyz / pers_pos.w; //TODO Remove ?
    vec2 uv = (pers_pos.xyz/ pers_pos.w).xy * 0.5 + 0.5;
    vec3 sample_view_pos = pos_from_depth(uv);

    vec3 p_to_sample = sample_view_pos - p;
    float dist = length(p_to_sample);
    float range_check = smoothstep(0.0, 1.0, dist/u_ssao_settings.kernel_radius);
    //Check how much we occlude and scale by distance from sampling point "p"
    ao += max(0.0, dot(n, normalize(p_to_sample)) * (1.0 - range_check));
  }

  ao /= u_ssao_settings.kernel_size;

  final_ao = ao;

}
