#version 450

//The image
layout(set = 0, binding = 0) uniform sampler2D color_input;

///Exposure we'll use to check if we fall within the hdr buffer or not
layout(set = 0, binding = 1) buffer LumiosityBuffer{
  ///Contains the lumiosity of the last 100 frames
  float lum_histogram[100];
  uint last_idx;
  float exposure;
} u_lum_buf;


//Get the uvs
layout(location = 0) in vec2 inter_coord;
layout(location = 1) in vec2 v_pos;


//outputs the fragment color
layout(location = 0) out vec4 out_hdr;


void main()
{

  //Get main hdr color
  vec4 resolved_color = texture(color_input, inter_coord);
  //Expose
  resolved_color *= u_lum_buf.exposure;

  //make greyscale and have a look at the brighness
  float brightness = dot(resolved_color.rgb, vec3(0.2126, 0.7152, 0.0722));
  //Sort hdr fragments
  if(brightness > 1.0 ){
    out_hdr = vec4(resolved_color.rgb, 1.0);
  }else{
    out_hdr = vec4(vec3(0.0), 0.0);
  }
}
