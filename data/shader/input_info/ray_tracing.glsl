#version 450


///Defines several structs which are used for the bvh and its traversal

//A generic ray
struct Ray
{
  vec3 origin;
  vec3 direction;
};

// -2 = no node
// -1 = no escape aka. finished traversing
struct BvhNode
{
  //min and max of the Bvh Node
  vec4 aabb_min;
  vec4 aabb_max;
  //Index of right and left child within the bvh buffer. If one of the indexes is -2 it means
  //that there is no children.
  int left;
  int right;
  //Index where we have to jump to if we don't intersect this node
  int esc_idx;
  //Index of this node in the array
  int idx;

  //If it is a leave this is true ( ==1 ) and the left will contain the index of the
  // first triangle in the meshes Buffer and right the count
  uint isLeaf;
  uint _pad0;
  uint _pad1;
  uint _pad2;
};

struct Mesh
{
  mat4 modelMatrix;
  mat4 invModelMatrix;
  //Points to the first index in the array of mesh specific bvh nodes
  uint bottom_bvh_offset;
  ///Length of this mesh bvh buffers
  uint bottom_bvh_length;
  ///Contains the offset in the faces buffer we need to respect when indexing the
  ///Faces buffer.
  uint face_offset;
  ///Contains the offset to the vertices we have to respect when indexing into
  ///the vertex buffer
  uint vert_offset;
  //TODO material information
};

//Indexs into the vertex buffer which vertices make up this face of the mesh
struct Face{
  uint vert0;
  uint vert1;
  uint vert2;
  uint _pad0;
};

//A single vertex
struct Vertex{
  vec4 pos;
  vec2 uv;
  float _pad0;
  float _pad1;
  vec4 normal;
  vec4 tangent;
};

layout(set = 1, binding = 0) readonly buffer TopBvh {
  //assuming that 0 is the root node
  BvhNode nodes[];
} u_top_bvh;

layout(set = 1, binding = 1) readonly buffer Meshes {
  Mesh meshes[];
} u_meshes;

///The mesh specific bvh buffers, index offsets are stored per mesh
/// A leafs index+mesh_specific_face_offset = the face this leaf represents
layout(set = 1, binding = 2) readonly buffer BottomBvh {
  BvhNode nodes[];
} u_bottom_bvh;

layout(set = 1, binding = 3) readonly buffer Faces {
  Face faces[];
} u_faces;

layout(set = 1, binding = 4) readonly buffer VertexBuffer {
  Vertex verts[];
} u_vertex;

/*
struct Tris
{
  vec4 p_one;
  vec4 p_two;
  vec4 p_three;
};

layout(set = 0, binding = 5) readonly buffer TrisBuffer {
  //assuming that 0 is the root node
  Tris tris[];
} u_tris;
*/
void main(){
  //Do nothing
}
