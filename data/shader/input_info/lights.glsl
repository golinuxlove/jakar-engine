#version 450

//LIGHTS
//==============================================================================
struct PointLight
{
  vec4 color;
  vec4 location;
  float intensity;
  float radius;
  float _pad0;
  float _pad1;
  int shadow_idx;
  int _pad2;
  int _pad3;
  int _pad4;
};

struct DirectionalLight
{
  vec4 color;
  vec4 direction;
  float intensity;
  float _pad0;
  float _pad1;
  float _pad2;
  int shadow_idx;
  int _pad3;
  int _pad4;
  int _pad5;
};

struct SpotLight
{
  vec4 color;
  vec4 direction;
  vec4 location;

  float intensity;
  float radius;
  float outer_radius;
  float inner_radius;
  int shadow_idx;
  int _pad0;
  int _pad1;
  int _pad2;
};

layout(set = 3, binding = 0) readonly buffer point_lights{
  PointLight p_light[];
}u_point_light;
//==============================================================================
layout(set = 3, binding = 1) readonly buffer directional_lights{
  DirectionalLight d_light[];
}u_dir_light;
//==============================================================================

layout(set = 3, binding = 2) readonly buffer spot_lights{
  SpotLight s_light[];
}u_spot_light;
//==============================================================================

//descibes the count of lights used
layout(set = 3, binding = 3) uniform LightCount{
  uint points;
  uint directionals;
  uint spots;
  uint _pad0;
}u_light_count;
//==============================================================================

void main(){}
